package information;

import java.awt.geom.Point2D;

/**
 * Information contenant la position d'une voiture
 * 
 * @author _Philippe_
 *
 */
public class CarPosition extends Information {

	private Point2D.Double position;
	private static final int INFO_TYPE_ID = 1;

	/**
	 * cree un CarPosition
	 * 
	 * @param currentTime
	 *            le temps a la creation de la voiture
	 * @param position
	 *            la position de la voiture observee
	 * @param sumoId l'identifiant de la voiture decrite par l'information
	 */
	public CarPosition(int currentTime, Point2D.Double position, String sumoId) {
		super(currentTime, sumoId + INFO_TYPE_ID);
		this.position = position;
	}

	/**
	 * retourne la position du vehicule
	 * 
	 * @return la position du vehicule
	 */
	public Point2D.Double getPosition() {
		return position;
	}
	

}
