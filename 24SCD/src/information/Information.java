package information;

/**
 * Classe abstraite qui regroupe les informations transmissibles via messages DSRC
 * @author _Philippe_
 *
 */
public abstract class Information {

	private int decayTime;
	private static int durationTime = 1000;
	private String id;

	/**
	 * cree une Information
	 * @param currentTime le temps a la creation de l'information
	 * @param id l'identifiant de l'information
	 */
	public Information(int currentTime, String id) {
		this.decayTime = currentTime + durationTime;
		this.setId(id);
	}

	/**
	 * retourne le decayTime, soit le temps ou l'information sera jugee trop vieille pour etre prise en compte
	 * @return le temps ou l'information sera jugee trop vieille pour etre prise en compte
	 */
	public int getDecayTime() {
		return decayTime;
	}

	/**
	 * permet de fixer le decayTime
	 * @param decayTime le temps a fixer pour le decayTime, soit le temps ou l'information sera jugee trop vieille pour etre prise en compte
	 */
	public void setDecayTime(int decayTime) {
		this.decayTime = decayTime;
	}

	/**
	 * Change le temps de vie d'une information
	 * @param durationTimeToChange le temps de vie de l'information, en ms
	 */
	public static void setDurationTime(int durationTimeToChange) {
		durationTime = durationTimeToChange;
	}

	/**
	 * @return l'identifiant de l'information
	 */
	public String getId() {
		return id;
	}

	/**
	 * defini l'identifiant de l'information
	 * @param id le nouvel identifiant de l'information
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	public String getVehicleId(){
		return this.getId().substring(0, this.getId().length()-1);
	}
	
	public static int getDurationTime(){
		return durationTime;
	}

}
