package information;

import java.awt.geom.Area;

/**
 * Un type d'information utile a la simulation Ce genre d'information ne serait
 * pas reellement transmis par des communications DSRC, cependant, il nous
 * permet d'afficher la zone d'acces a l'information
 * 
 * @author _Philippe_
 *
 */
public class ZoneInformation extends Information {

	private Area informationAccessZone;
	private static final int INFO_TYPE_ID = 2;

	/**
	 * Cree une ZoneInformaiton
	 * 
	 * @param currentTime
	 *            le temps a la creation de l'information
	 * @param informationAccessZone
	 *            la zone d'acces a l'information du vehicule envoyant le
	 *            message
	 * @param sumoId l'identifiant du vehicule dont la zone de detection est contenue dans le message
	 */
	public ZoneInformation(int currentTime, Area informationAccessZone, String sumoId) {
		super(currentTime, sumoId + INFO_TYPE_ID);
		this.informationAccessZone = new Area(informationAccessZone);
	}

	/**
	 * retourne la zone contenue par l'information
	 * 
	 * @return la zone de detection de l'information
	 */
	public Area getInformationAccessZone() {
		return informationAccessZone;
	}

}
