package connection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import aapplication.ScvAnimation;
import infrastructure.Antenna;
import vehicle.DsrcVehicle;
import vehicle.PhysicLayer;
import vehicle.ScvVehicle;

/**
 * Une connection contient un PhysicLayer sender ainsi qu'une liste des
 * PhysicLayer avec lesquelles il a reussi a se connecter
 * 
 * @author _Philippe_
 *
 */
public class Connection {

	private PhysicLayer sender;
	private List<PhysicLayer> receivers;
	private static final int MAX_DISTANCE = 350;

	/**
	 * cree une connection
	 * 
	 * @param sender
	 *            le PhysicLayer du vehicle qui souhaite etablir une connection
	 */
	public Connection(PhysicLayer sender) {
		this.sender = sender;
		calculateReceivers();
	}

	/**
	 * 
	 * Vide la liste des receivers et calcule si la connection est reussie avec
	 * toutes les automobiles � l'interieur de 350m de rayon
	 * 
	 */
	public void calculateReceivers() {
		emptyReceivers();
		DsrcVehicle dsrcVehicle;
		for (ScvVehicle genericVehicle : ScvAnimation.getVehicles()) {
			if (genericVehicle.isDsrc()) {
				dsrcVehicle = (DsrcVehicle) genericVehicle;
				if (sender.getPosition().distance(dsrcVehicle.getPosition()) <= MAX_DISTANCE && !sender.equals(dsrcVehicle.getPhysicLayer())) {
					receivers.add(dsrcVehicle.getPhysicLayer());
				}
			}
		}
		for (Antenna antenna : ScvAnimation.getAntennas()) {
				if (sender.getPosition().distance(antenna.getPosition()) <= MAX_DISTANCE && !sender.equals(antenna.getPhysicLayer())) {
					receivers.add(antenna.getPhysicLayer());
				}
		}
	}

	/**
	 * remplace la liste des receivers avec une liste vide
	 */
	public void emptyReceivers() {
		receivers = Collections.synchronizedList(new ArrayList<PhysicLayer>());
	}

	/**
	 * retourne la liste des PhysicLayer avec lesquels la connection a ete
	 * etablie
	 * 
	 * @return la liste des receivers
	 */
	public List<PhysicLayer> getReceivers() {
		return receivers;
	}

	/**
	 * retourne le PhysicLayer du vehicule qui etabli la connection
	 * 
	 * @return le PhysicLayer du vehicule qui etabli la connection
	 */
	public PhysicLayer getSender() {
		return sender;
	}

}
