package aapplication;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.EventListenerList;

import listener.PresentationWindowClosedListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Cette classe permet la creation d'une fenetre de bienvenue a l'ouverture de l'application
 * Elle est purement esthetique
 * 
 * @author _Philippe_
 *
 */
public class PresentationWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int WIDTH = 650;
	private final int HEIGHT = 430;
	
	private final EventListenerList SAVED_OBJECTS= new EventListenerList();

	private ImagePanel contentPane;

	/**
	 * Create the frame.
	 */
	public PresentationWindow() {
		setUndecorated(true);
		setType(Type.POPUP);
		setAlwaysOnTop(true);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setBounds((int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth() - WIDTH) / 2,
				(int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight() - HEIGHT) / 2, WIDTH, HEIGHT);
		
		URL imgUrl = getClass().getClassLoader().getResource("PresentationWindow.png");
		try {
			contentPane = new ImagePanel(ImageIO.read(imgUrl));
			contentPane.setFocusable(true);
			contentPane.requestFocusInWindow();
			contentPane.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					switchToMainApp();
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblScd = new JLabel("S.C.V.");
		lblScd.setHorizontalTextPosition(SwingConstants.CENTER);
		lblScd.setFont(new Font("Vani", Font.BOLD, 99));
		lblScd.setHorizontalAlignment(SwingConstants.CENTER);
		lblScd.setBounds(165, 56, 319, 137);
		contentPane.add(lblScd);

		JLabel lblsimulateurDeCommunications = new JLabel("(Simulateur de Communications V\u00E9hiculaires)");
		lblsimulateurDeCommunications.setHorizontalAlignment(SwingConstants.CENTER);
		lblsimulateurDeCommunications.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblsimulateurDeCommunications.setBounds(143, 192, 363, 36);
		contentPane.add(lblsimulateurDeCommunications);

		JButton btnCommencer = new JButton("Commencer");
		btnCommencer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				switchToMainApp();
			}
		});
		btnCommencer.setBounds(268, 325, 113, 23);
		contentPane.add(btnCommencer);

		JTextPane txtpnTest = new JTextPane();
		txtpnTest.setEditable(false);
		txtpnTest.setText("Travail par C\u00E9dryk Doucet et Philippe Alexandre");
		txtpnTest.setBounds(247, 239, 156, 45);
		contentPane.add(txtpnTest);
	}
	
	/**
	 * Permet le fonctionnement de l'ecouteur personnalise qui fait l'echange entre la fenetre de presentation et la fenetre principale
	 * @param objEcout un listener qui decrit les methodes a accomplir s'il est appele
	 */
	public void addPresentationWindowClosedListener(PresentationWindowClosedListener objEcout){
		SAVED_OBJECTS.add(PresentationWindowClosedListener.class, objEcout);
	}
	
	/**
	 * ferme la fenetre de presentation et affiche l'application
	 */
	private void switchToMainApp(){
		dispose();
		for(PresentationWindowClosedListener listener : SAVED_OBJECTS.getListeners(PresentationWindowClosedListener.class)){
			listener.presentationWindowClosed();
		}
	}
}
