package aapplication;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

/**
 * Un panneau dessinant une image
 * 
 * @author Cedryk
 *	
 */
public class ImagePanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5152932577025783752L;
	private Image image;
	
	/**
	 * Create the panel.
	 * @param image l'image a afficher
	 */
	public ImagePanel(Image image) {
		this.image = image;
	}
	
	/**
	 * dessine le composant
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 **/
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.drawImage(image.getScaledInstance(getWidth(), getHeight(), Image.SCALE_FAST), 0, 0, null);
	}

}
