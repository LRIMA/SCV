package aapplication;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.geom.Point2D.Double;
import java.io.PrintStream;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;

import charts.ChartFrame;
import charts.Statistic;
import event.DsrcPeriodic;
import event.Update;
import information.Information;
import infrastructure.Antenna;
import infrastructure.VehicleTrustPoints;
import listener.ContentPaneListener;
import listener.UpdateListener;
import obstacle.ObstacleRect;
import vehicle.DsrcVehicle;
import vehicle.PhysicLayer;
import vehicle.ScvVehicle;
import vehicle.VehicleRadioState;
import javax.swing.JTable;
import javax.swing.JScrollBar;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 * Panneau ou se trouve le panneau d'animation
 * 
 * @author _philippe_cedryk_
 *
 */
public class AnimationFrame extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8106715589321724613L;
	private ScvAnimation scvAnimation;
	private JButton btnPlayPause;
	private JTextArea animationConsole;
	private ConsoleTextArea consoleStream;
	private JLabel lblNoCarSelected;
	private JPanel pnlCarInfo;
	private JScrollPane consoleScroller;
	private JButton btnStep;
	private JSlider slderAnimSpeed;
	private JLabel lblAnimSpeed;
	private JToggleButton tglbtnConsole;
	private JToggleButton tglbtnInfrastructure;
	private JPanel pnlInfra;
	private final String ANIM_SPEED_TEXT = "Vitesse d'animation : ";
	private final String CAR_SPEED_TEXT = "Vitesse : ";
	private final String CAR_POSX_TEXT = "Position en X : ";
	private final String CAR_POSY_TEXT = "Position en Y : ";
	private final String CAR_NAME_TEXT = "Nom : ";
	private final String CAR_DSRC_TEXT = "Utilise la technologie DSRC";
	private final String RADIO_MODE_TEXT = "Mode de la radio :";
	private final String NOISE = "Bruit :";
	private final String MALICIOUS = "V�hicule malicieux :";
	private final String TRUST_POINTS = "Points de confiance :";
	private JButton btnReset;

	private String cfgFileName;

	private final EventListenerList SAVED_OBJECTS = new EventListenerList();
	private JLabel lblXPos;
	private JLabel lblSpeed;
	private JLabel lblYPos;
	private JLabel lblVehicleName;
	private JLabel lblDsrc;
	private JLabel lblCrash;
	private JButton btnCrash;
	private JButton btnRemAnt;
	private JButton btnCharts;
	private ChartFrame charts;
	private JButton btnLaunchFrame;
	private JLabel lblRadioMode;
	private JLabel lblNoise;
	private Statistic[] firstStatistics;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JToggleButton tglbtnAntenna;
	private JLabel lblMalicious;
	private JLabel lblTrustPoints;
	private Object[][] vehiclesData;
	private String[] headers  = {"Nom", "Malicieux", "Points", "Detecte"};
	private JTable table;
	private JScrollPane scrollPane;
	private int maliciousDetected = 0;
	private boolean done;

	/**
	 * 
	 * @param width
	 *            la largeur du paneau
	 * @param upMargin
	 *            la marge du haut , d�terminee par le menu
	 */
	// auteur: philippe et cedryk
	public AnimationFrame(int width, int upMargin) {
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_SPACE) {
					playPause();
				}
			}
		});
		setLayout(null);

		scvAnimation = new ScvAnimation();
		scvAnimation.setBounds(280, upMargin, width - 310, 480);
		add(scvAnimation);

		btnPlayPause = new JButton("D�marrer");
		btnPlayPause.setBounds(280, 514, 159, 23);
		btnPlayPause.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				playPause();
			}
		});
		add(btnPlayPause);

		// charts = new ChartFrame();
		// charts.setVisible(false);

		pnlCarInfo = new JPanel();
		pnlCarInfo.setBounds(10, upMargin, 260, 515);
		pnlCarInfo.setBorder(new TitledBorder(null, "Informations sur le v\u00E9hicule s\u00E9lectionn\u00E9",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		add(pnlCarInfo);
		pnlCarInfo.setLayout(null);

		lblNoCarSelected = new JLabel("Veuillez s\u00E9lectionner un v\u00E9hicule...");
		lblNoCarSelected.setHorizontalAlignment(SwingConstants.CENTER);
		lblNoCarSelected.setBounds(10, 256, 240, 14);
		pnlCarInfo.add(lblNoCarSelected);

		lblXPos = new JLabel("Position en X : ");
		lblXPos.setBounds(10, 178, 240, 14);
		pnlCarInfo.add(lblXPos);
		lblXPos.setVisible(false);

		lblSpeed = new JLabel("Vitesse :");
		lblSpeed.setBounds(10, 256, 240, 14);
		pnlCarInfo.add(lblSpeed);
		lblSpeed.setVisible(false);

		lblYPos = new JLabel("Position en Y :");
		lblYPos.setBounds(10, 203, 240, 14);
		pnlCarInfo.add(lblYPos);
		lblYPos.setVisible(false);

		lblVehicleName = new JLabel(CAR_NAME_TEXT);
		lblVehicleName.setBounds(10, 91, 240, 14);
		pnlCarInfo.add(lblVehicleName);

		lblDsrc = new JLabel(CAR_DSRC_TEXT);
		lblDsrc.setBounds(10, 116, 240, 14);
		lblDsrc.setVisible(false);
		pnlCarInfo.add(lblDsrc);

		btnCrash = new JButton("Accident");
		btnCrash.setVisible(false);
		btnCrash.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				scvAnimation.crash();
			}
		});
		btnCrash.setBounds(76, 481, 89, 23);
		pnlCarInfo.add(btnCrash);

		btnRemAnt = new JButton("Effacer");
		btnRemAnt.setVisible(false);
		btnRemAnt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scvAnimation.removeSelectedAntenna();
			}
		});
		btnRemAnt.setBounds(76, 481, 89, 23);
		pnlCarInfo.add(btnRemAnt);

		lblCrash = new JLabel("\u00C9tat du v\u00E9hicule:");
		lblCrash.setBounds(10, 445, 240, 14);
		lblCrash.setVisible(false);
		pnlCarInfo.add(lblCrash);

		lblRadioMode = new JLabel("Mode de la radio :");
		lblRadioMode.setHorizontalAlignment(SwingConstants.LEFT);
		lblRadioMode.setBounds(10, 318, 240, 14);
		lblRadioMode.setVisible(false);
		pnlCarInfo.add(lblRadioMode);

		lblNoise = new JLabel("Bruit :");
		lblNoise.setHorizontalAlignment(SwingConstants.LEFT);
		lblNoise.setBounds(10, 343, 240, 14);
		lblNoise.setVisible(false);
		pnlCarInfo.add(lblNoise);
		
		lblMalicious = new JLabel("V\u00E9hicule malicieux :");
		lblMalicious.setBounds(10, 368, 240, 14);
		lblMalicious.setVisible(false);
		pnlCarInfo.add(lblMalicious);
		
		lblTrustPoints = new JLabel("Points de confiance :");
		lblTrustPoints.setBounds(10, 393, 240, 14);
		lblTrustPoints.setVisible(false);
		pnlCarInfo.add(lblTrustPoints);

		lblVehicleName.setVisible(false);

		consoleScroller = new JScrollPane();
		consoleScroller
				.setBorder(new TitledBorder(null, "Console", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		consoleScroller.setBounds(10, 570, 1457, 206);
		add(consoleScroller);

		animationConsole = new JTextArea();
		consoleScroller.setViewportView(animationConsole);
		animationConsole.setLineWrap(true);
		animationConsole.setEditable(false);

		consoleStream = new ConsoleTextArea(animationConsole);
		System.setOut(new PrintStream(consoleStream));

		btnStep = new JButton("Pas \u00E0 pas");
		btnStep.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (scvAnimation.isConnectionUp() && scvAnimation.animationIsRunning()) {
					playPause();
				} else {
					startSimulation(false);
					nextSimStep();
				}
			}
		});
		btnStep.setBounds(449, 514, 159, 23);
		add(btnStep);

		slderAnimSpeed = new JSlider();
		slderAnimSpeed.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				int value = slderAnimSpeed.getValue();
				if (value == 0) {
					slderAnimSpeed.setValue(1);
					value = 1;
				}
				scvAnimation.changeAnimationSpeed(value);
				lblAnimSpeed.setText(ANIM_SPEED_TEXT + value + "x");
			}
		});

		lblAnimSpeed = new JLabel(ANIM_SPEED_TEXT);
		lblAnimSpeed.setBounds(649, 518, 160, 14);
		add(lblAnimSpeed);

		slderAnimSpeed.setValue(5);
		slderAnimSpeed.setPaintLabels(true);
		slderAnimSpeed.setSnapToTicks(true);
		slderAnimSpeed.setPaintTicks(true);
		slderAnimSpeed.setMinorTickSpacing(1);
		slderAnimSpeed.setMajorTickSpacing(5);
		slderAnimSpeed.setMaximum(20);
		slderAnimSpeed.setBounds(791, 502, 272, 45);
		add(slderAnimSpeed);

		btnReset = new JButton("R\u00E9initialiser");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetSimulation();
			}
		});
		btnReset.setBounds(1064, 514, 139, 23);
		add(btnReset);

		btnLaunchFrame = new JButton("Retourner \u00E0 la page de lancement");
		btnLaunchFrame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for (ContentPaneListener listener : SAVED_OBJECTS.getListeners(ContentPaneListener.class)) {
					listener.switchContentPane();
				}
			}
		});
		btnLaunchFrame.setBounds(1213, 502, 254, 23);
		add(btnLaunchFrame);

		btnCharts = new JButton("Ouvrir la page de graphiques");
		btnCharts.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if (charts == null) {
					charts = new ChartFrame();
					charts.statisticsChange(true, firstStatistics);
				} else if (charts.getState() == JFrame.ICONIFIED) {
					charts.setState(JFrame.NORMAL);
					charts.toFront();
				} else {
					charts.setVisible(true);
					charts.toFront();
				}
			}
		});
		btnCharts.setBounds(1213, 527, 254, 23);
		add(btnCharts);

		tglbtnConsole = new JToggleButton("Console");
		tglbtnConsole.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tglbtnConsole.isSelected()) {
					pnlInfra.setVisible(false);
					consoleScroller.setVisible(true);

				}
			}
		});
		tglbtnConsole.setSelected(true);
		buttonGroup.add(tglbtnConsole);
		tglbtnConsole.setBounds(10, 536, 81, 23);
		add(tglbtnConsole);

		tglbtnInfrastructure = new JToggleButton("Infrastructure");
		tglbtnInfrastructure.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tglbtnInfrastructure.isSelected()) {
					consoleScroller.setVisible(false);
					pnlInfra.setVisible(true);
				}
			}
		});
		buttonGroup.add(tglbtnInfrastructure);
		tglbtnInfrastructure.setBounds(90, 536, 125, 23);
		add(tglbtnInfrastructure);

		pnlInfra = new JPanel();
		pnlInfra.setVisible(false);
		pnlInfra.setBorder(
				new TitledBorder(null, "Infrastructure", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlInfra.setBounds(10, 570, 1457, 206);
		add(pnlInfra);
		pnlInfra.setLayout(null);

		tglbtnAntenna = new JToggleButton("Ajouter une antenne");
		tglbtnAntenna.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tglbtnAntenna.isSelected()) {
					scvAnimation.addAntenna(true);
				} else {
					scvAnimation.addAntenna(false);
				}
			}
		});
		tglbtnAntenna.setBounds(20, 40, 150, 23);
		pnlInfra.add(tglbtnAntenna);
		
		table = new JTable(new Object[1][4], headers);
		TableModel defaultModel = new DefaultTableModel(new Object[1][4], headers);
		table.setModel(defaultModel);
		table.setRowSelectionAllowed(false);
		table.setBounds(800, 21, 647, 174);
		table.setEnabled(false);
		scrollPane = new JScrollPane(table);
		scrollPane.setBounds(800, 21, 647, 174);
		pnlInfra.add(scrollPane);

		scvAnimation.addUpdateListener(new UpdateListener() {


			public void carInformationUpdated(Double position, double speed, VehicleRadioState radioState,
					double noise, java.lang.Double trustPoints) {
				lblXPos.setText(CAR_POSX_TEXT + " " + String.format("%.2f", position.getX()) + " m");
				lblYPos.setText(CAR_POSY_TEXT + " " + String.format("%.2f", position.getY()) + " m");
				lblSpeed.setText(CAR_SPEED_TEXT + " " + String.format("%.3f", speed) + " m/s ("
						+ String.format("%.1f", mSToKmH(speed)) + " km/h)");
				lblTrustPoints.setText(TRUST_POINTS + " "+ String.format("%.2f",trustPoints));
				String radioStateText = " ";
				if (radioState != null) {
					switch (radioState) {
					case WAITING:
						radioStateText = " en attente";
						break;
					case SENDING:
						radioStateText = " en diffusion";
						break;
					case LISTENING:
						radioStateText = " en reception";
						break;
					}
					lblRadioMode.setText(RADIO_MODE_TEXT + radioStateText);
					lblNoise.setText(NOISE + " " + String.format("%.3f", noise) + " W");
					lblRadioMode.setVisible(true);
					lblNoise.setVisible(true);
				} else {
					lblRadioMode.setVisible(false);
					lblNoise.setVisible(false);
				}
			}

			@Override
			public void carClicked(boolean selected, String carName, boolean isDsrc, boolean isMalicious) {
				showCarInfo(selected);
				lblVehicleName.setText(CAR_NAME_TEXT + carName);
				if (isDsrc){
					lblDsrc.setVisible(true);
					lblMalicious.setVisible(true);
					if(isMalicious){
						lblMalicious.setText(MALICIOUS + " oui");
					}else{
						lblMalicious.setText(MALICIOUS + " non");
						lblTrustPoints.setVisible(true);
					}
				}
				else{
					lblDsrc.setVisible(false);
					lblMalicious.setVisible(false);
					lblTrustPoints.setVisible(false);
				}
				
				
			}

			public void carIsCrashed(boolean isCrashed) {
				refreshCrashLabel(isCrashed);
			}

			@Override
			public void chartsValuesUpdated(boolean carChanged, Statistic... statistics) {
				if (charts != null) {
					charts.statisticsChange(carChanged, statistics);
				} else {
					firstStatistics = statistics;
				}
			}

			@Override
			public void antennaPlaced() {
				tglbtnAntenna.setSelected(false);

			}

			@Override
			public void antennaClicked(boolean selected, Antenna antenna, String antennaID) {
				showAntennaInfo(selected);
				if (antenna != null) {
					lblVehicleName.setText(CAR_NAME_TEXT + antennaID);
					lblXPos.setText(CAR_POSX_TEXT + " " + String.format("%.2f", antenna.getPosition().getX()) + " m");
					lblYPos.setText(CAR_POSY_TEXT + " " + String.format("%.2f", antenna.getPosition().getY()) + " m");
				}
			}

			@Override
			public void simulationUpdated(ArrayList<DsrcVehicle> vehicles) {
				vehiclesData = new Object[vehicles.size()][headers.length];
				int nbDetected = 0;
				int nbMalicious = 0;
				for(int i = 0; i < vehicles.size(); i++){
					DsrcVehicle vehicle = vehicles.get(i);
					for(int j = 0; j < headers.length; j++){
						Object obj = null;
						switch(j){
						case 0:
							obj = vehicle.getSumoVehicle().getID();
							break;
						case 1:
							obj = vehicle.isMalicious();
							if((boolean) obj)
								nbMalicious++;
							break;
						case 2:
							obj = VehicleTrustPoints.getMap().get(vehicle.getSumoVehicle().getID());
							break;
						case 3:
							obj = VehicleTrustPoints.getMap().get(vehicle.getSumoVehicle().getID()) <= 0.3;
							if ((boolean)obj)
								nbDetected++;
							break;
						default:
							break;
						}
						vehiclesData[i][j] = obj;
					}
				}
				TableModel model = new  DefaultTableModel(vehiclesData, headers);
				table.setModel(model);
				if(ScvAnimation.getVehicles().size() == 60 && !done){
					System.out.println("Tous les vehicules sont presents : " + ScvAnimation.getTime());
					done = !done;
				}
				if(nbDetected >= nbMalicious/2 && maliciousDetected == 0 && nbMalicious > 20){
					if(nbMalicious != 1)
					System.out.println("La moitie des vehicules a ete detectee : "+ ScvAnimation.getTime());
					maliciousDetected ++;
				}else if(nbDetected == nbMalicious && maliciousDetected == 1 && done){
					System.out.println("Tous les vehicules ont ete detectes : "+ ScvAnimation.getTime());
					maliciousDetected ++;
				}
			}

		});
		
		
		
	}

	/**
	 * Methode qui permet de determiner si les images seront utilis�es lors du
	 * dessin
	 * 
	 * @param vehicleImage
	 *            vrai si les images sont utilisees pour les vehicules, faux
	 *            sinon
	 * @param buildingImage
	 *            vrai si les images sont utilisees pour les batiments, faux
	 *            sinon
	 */
	// auteur : Cedryk
	public void setImageDrawing(boolean vehicleImage, boolean buildingImage) {
		ObstacleRect.setImages(buildingImage);
		ScvVehicle.setImages(vehicleImage);
	}

	/**
	 * demare la simulation
	 * 
	 * @param animate
	 *            booleen pour controler si l'animation demarre tout de suite
	 */
	// auteur: cedryk
	public void startSimulation(boolean animate) {
		scvAnimation.start(animate);
	}

	/**
	 * Arrete l'animation
	 */
	// auteur: cedryk
	public void closeSimulation() {
		scvAnimation.closeSimulation();

	}

	/**
	 * Mets l'animation en pause
	 */
	// auteur: cedryk
	private void pauseSimulation() {
		scvAnimation.pauseSimulation();
	}

	/**
	 * Passe a la prochaine etape dans la simulation
	 */
	// auteur: cedryk
	private void nextSimStep() {
		scvAnimation.stepByStep();
	}

	/**
	 * reinitialise la simulation
	 */
	// auteur: cedryk
	protected void resetSimulation() {
		closeSimulation();
		scvAnimation.initialize(slderAnimSpeed.getValue(), cfgFileName);
		btnPlayPause.setText("D�marrer");
		showCarInfo(false);
		if (charts != null) {
			charts.resetChart();
		}
		maliciousDetected = 0;
		done = false;
	}

	/**
	 * Permet de rajouter le listener pour retourner a la page de lancement
	 * 
	 * @param objEcout
	 *            le listener
	 */
	// auteur : Philippe
	public void addContentPaneChangeListener(ContentPaneListener objEcout) {
		SAVED_OBJECTS.add(ContentPaneListener.class, objEcout);
	}

	/**
	 * permet de determiner les parametres de simulation
	 * 
	 * @param updateFrequency
	 *            la frequence de mise a jour des positions
	 * @param durationTime
	 *            la duree de vie des informations Dsrc
	 * @param dsrcPercentage
	 *            le pourcentage de vehicules Dsrc
	 * @param messagesFrequency
	 *            la frequence d'envoi de messages Dsrc
	 * @param sensitivity
	 *            la sensibilite des radios des vehicules
	 * @param maliciousPercentage 
	 */
	// auteur: Cedryk
	public void setSimulationOptions(int updateFrequency, int durationTime, int dsrcPercentage, int messagesFrequency,
			double sensitivity, int maliciousPercentage) {
		scvAnimation.setUpdateFrequency(updateFrequency);
		Information.setDurationTime(durationTime);
		Update.setDsrcPercentage(dsrcPercentage);
		Update.setMaliciousPercentage(maliciousPercentage);
		DsrcPeriodic.setMessagesFrequency(messagesFrequency);
		PhysicLayer.setSensitivity(sensitivity);
	}

	/**
	 * Determine si on doit afficher les informations d'une automobile
	 * selectionnee
	 * 
	 * @param show
	 *            vrai si une automobile est selectionnee, faux sinon
	 */
	// auteur: Cedryk
	private void showCarInfo(boolean show) {
		lblXPos.setVisible(show);
		lblYPos.setVisible(show);
		lblSpeed.setVisible(show);
		lblVehicleName.setVisible(show);
		lblDsrc.setVisible(show);
		btnCrash.setVisible(show);
		lblCrash.setVisible(show);

		lblNoise.setVisible(show);
		lblRadioMode.setVisible(show);
		lblMalicious.setVisible(show);
		lblTrustPoints.setVisible(show);
		lblNoCarSelected.setVisible(!show);
	}

	/**
	 * Transforme une valeur de metres par seconde en kilometres heure
	 * 
	 * @param ms
	 *            metres par seconde
	 * @return kilometres heure
	 */
	// auteur: Cedryk
	private double mSToKmH(double ms) {
		return ms * 3.6;
	}

	/**
	 * determine la frequence de mise a jour des informations du vehicule
	 * selectionne
	 * 
	 * @param infoUpdateFrequency
	 *            la frequence de mise a jour des informations du vehicule
	 *            selectionne
	 */
	// auteur: Cedryk
	public void setGeneralOptions(int infoUpdateFrequency) {
		scvAnimation.setInfoUpdateFrequency(infoUpdateFrequency);
	}

	/**
	 * met a jour les details de l'etat du vehicule selectionne
	 * 
	 * @param isCrashed
	 *            vrai si le vehicule selectionner est accidente, faux sinon
	 */
	private void refreshCrashLabel(boolean isCrashed) {
		if (isCrashed) {
			btnCrash.setText("R�parer");
			lblCrash.setText("\u00C9tat du v\u00E9hicule: accident\u00E9 ");
		} else {
			btnCrash.setText("Accident");
			lblCrash.setText("\u00C9tat du v\u00E9hicule: fonctionnel ");
		}
	}

	/**
	 * initialise le composant d'animation suite a son ouverture
	 * 
	 * @param cfgFileName
	 *            le nom du fichier de configuration
	 */
	public void initializeAnimationComponent(String cfgFileName) {
		scvAnimation.initialize(5, cfgFileName);
		this.cfgFileName = cfgFileName;
	}

	/**
	 * demarre ou pause l'application et change ce que le bouton demarrer/pause
	 * affiche en fonction de l'etat de l'animation
	 */
	private void playPause() {
		if (!scvAnimation.animationIsRunning()) {
			startSimulation(true);
			btnPlayPause.setText("Pause");
		} else {
			pauseSimulation();
			btnPlayPause.setText("D�marrer");
		}
	}

	private void showAntennaInfo(boolean show) {
		lblXPos.setVisible(show);
		lblYPos.setVisible(show);
		lblVehicleName.setVisible(show);
		lblSpeed.setVisible(false);
		lblNoise.setVisible(false);
		lblRadioMode.setVisible(false);
		lblMalicious.setVisible(false);
		lblTrustPoints.setVisible(false);
		lblNoCarSelected.setVisible(!show);
		btnRemAnt.setVisible(show);
	}
}
