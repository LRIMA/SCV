package aapplication;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.event.EventListenerList;

import connection.Connection;
import connection.ConnectionManager;
import event.DsrcPeriodicInfra;
import event.Event;
import event.EventList;
import event.Update;
import information.PreCrashInformation;
import infrastructure.Antenna;
import infrastructure.VehicleTrustPoints;
import information.CarPosition;
import information.Information;
import it.polito.appeal.traci.SumoTraciConnection;
import listener.UpdateListener;
import obstacle.Obstacle;
import obstacle.ObstacleRect;
import physics.PhysicModel;
import vehicle.DsrcVehicle;
import vehicle.PhysicLayer;
import vehicle.ScvVehicle;

/**
 * Panneau sur lequel se retrouve l'animation
 * 
 * @author _Philippe_Cedryk_
 *
 */
public class ScvAnimation extends JPanel implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -154255656999088654L;

	private static ArrayList<Obstacle> obstacles;
	private static List<ScvVehicle> vehicles;
	private static List<Antenna> antennas;
	private PhysicModel physicModel;
	private static ConnectionManager connectionManager;
	private static SumoTraciConnection traciConnection;
	private static int time;

	private static EventList events;
	private Thread proc;
	private boolean animationRunning = false, connectionUp = false, firstTime = true, showImages = true, placingInfra = false;
	private int updateFrequency, animationSpeed;
	private long stepLength, pastTime;
	private ScvVehicle selectedVehicle;
	private ScvVehicle hoveredVehicle;
	private Antenna selectedAntenna;
	private int zoom, infoUpdateFrequency = 5, increment;
	private Point initialPoint;

	private final double ZOOM_SCALE = 1.2;

	private int x1 = -243, x2 = 170, x3 = 492, x4 = 777, x5 = 1091, x6 = 1161, x7 = 1559, x8 = 1900, x9 = 2113;
	private int y1 = 232, y2 = -61, y3 = 46, y4 = -296, y5 = -537, y6 = -760;

	private final int ZOOM_MAX = 20, ORIGINAL_WIDTH = x9 - x1;
	private final double POSITION_ACCURACY = 0.1;
	private final Point2D.Double ORIGIN = new Point2D.Double(x1, y1 + 100);

	private final EventListenerList SAVED_OBJECTS = new EventListenerList();
	
	

	/**
	 * Create the panel.
	 */
	// auteur: Cedryk
	public ScvAnimation() {
		initialPoint = new Point(0,0);
		addMouseWheelListener(new MouseWheelListener() {
			public void mouseWheelMoved(MouseWheelEvent wheelEvent) {
				int nbRot = wheelEvent.getWheelRotation();
				zoom(nbRot, wheelEvent.getPoint());
				repaint();
			}
		});
		setBackground(Color.WHITE);
		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent mouseDragged) {
				translate(mouseDragged.getPoint(), initialPoint);
				repaint();
				initialPoint = mouseDragged.getPoint();
			}

			public void mouseMoved(MouseEvent mouseMoved) {
				if (physicModel != null) {
					Point2D.Double mousePosition = physicModel.componentPositionToReal(mouseMoved.getPoint());

					if (hoveredVehicle != null) {
						repaint();
						hoveredVehicle = null;
					}
					synchronized (vehicles) {
						for (ScvVehicle vehicle : vehicles) {
							if (vehicle.getCarCircle().contains(mousePosition)) {
								hoveredVehicle = vehicle;
								repaint();
							}
						}
					}
				}
			}
		});

		addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent mousePressed) {
				Point2D.Double clickPosition = physicModel.componentPositionToReal(mousePressed.getPoint());
				for (ScvVehicle vehicle : vehicles) {
					if (vehicle.getCarCircle().contains(clickPosition)) {
						if (selectedVehicle != vehicle) {
							selectedAntenna = null;
							selectedVehicle = vehicle;
							if(!animationRunning){
								repaint();
							}
							
							for (UpdateListener listener : SAVED_OBJECTS.getListeners(UpdateListener.class)) {
								if(selectedVehicle.isDsrc())
									listener.carClicked(true, selectedVehicle.toString(), true, ((DsrcVehicle) selectedVehicle).isMalicious());
								else
									listener.carClicked(true, selectedVehicle.toString(), false, false);
								vehicle.carInformationUpdated(listener);
								listener.carIsCrashed(selectedVehicle.isCrashed());
								if (selectedVehicle.isDsrc()) {
									updateChartsValues((DsrcVehicle) selectedVehicle, listener, true);

								}
							}
						} else {
							selectedVehicle = null;
							if(!animationRunning){
								repaint();
							}

							for (UpdateListener listener : SAVED_OBJECTS.getListeners(UpdateListener.class)) {
								listener.carClicked(false, "", false, false);
							}
						}
					}
				}
				for(Antenna ant : antennas){
					if(ant.getAntennaCircle().contains(clickPosition)){
						if(selectedAntenna != ant){
							selectedVehicle = null;
							selectedAntenna = ant;
							if(!animationRunning){
								repaint();
							}
							
							for (UpdateListener listener : SAVED_OBJECTS.getListeners(UpdateListener.class)) {
								listener.antennaClicked(true, ant, ant.getID());
							}
						}else{
							selectedAntenna = null;
							if(!animationRunning){
								repaint();
							}
							
							for (UpdateListener listener : SAVED_OBJECTS.getListeners(UpdateListener.class)) {
								listener.antennaClicked(false, null, "");
							}
						}
					}
				}
				initialPoint = mousePressed.getPoint();
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if(placingInfra && initialPoint.equals(e.getPoint())){
					
					placingInfra = false;
					for (UpdateListener listener : SAVED_OBJECTS.getListeners(UpdateListener.class)) {
						listener.antennaPlaced();
					}
					Antenna toAdd = new Antenna(physicModel.componentPositionToReal(e.getPoint()));
					antennas.add(toAdd);
					DsrcPeriodicInfra event = new DsrcPeriodicInfra(time, toAdd);
					addEvent(event);
					connectionManager.addConnection(toAdd);
					System.out.println("Placed "+ toAdd.getID());
					repaint();
				}
			}
		});

	}

	/**
	 * Zoom/dezoom la carte sur un point en particulier
	 * 
	 * @param nbRot
	 *            nombre de rotations de la molette de souris
	 * @param zoomCenter
	 *            point sur lequel le zoom/dezoom s'effectue
	 */
	// auteur : Cedryk
	public void zoom(int nbRot, Point zoomCenter) {
		if (Math.abs(zoom + nbRot) < ZOOM_MAX) {
			zoom += nbRot;

			physicModel.scaleOnPosition(Math.pow(ZOOM_SCALE, nbRot), zoomCenter);

		}
	}

	/**
	 * Effectue une translation de la carte d'un point � un autre
	 * 
	 * @param initialPoint
	 *            point initial de la translation en pixels
	 * @param finalPoint
	 *            point final de la translation en pixels
	 */
	// auteur: cedryk
	public void translate(Point initialPoint, Point finalPoint) {
		double dx = (finalPoint.getX() - initialPoint.getX());
		double dy = -(finalPoint.getY() - initialPoint.getY());
		physicModel.translate(dx, dy);
	}

	/**
	 * dessine les objets n�cessaires � l'�cran
	 * 
	 * @param g
	 *            le contexte graphique
	 */
	// auteur: Cedryk
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		if (firstTime) {
			physicModel = new PhysicModel(ORIGINAL_WIDTH, this.getWidth(), this.getHeight(), ORIGIN);
			firstTime = false;
		}

		AffineTransform mat = physicModel.getRealToComponent();

		drawEdges(g2d, mat);

		if (hoveredVehicle != null) {
			hoveredVehicle.drawHighlight(g2d, mat);
		}

		if (vehicles != null) {
			synchronized (vehicles) {
				Iterator<ScvVehicle> vehicleIterator = vehicles.iterator();
				while (vehicleIterator.hasNext()) {
					ScvVehicle vehicle = vehicleIterator.next();
					if (!vehicle.equals(selectedVehicle)) {
						vehicle.draw(g2d, mat);
					}
				}
			}
		}

		for (Obstacle obstacle : obstacles) {
			obstacle.draw(g2d, mat);
		}
		
		for(Antenna ant : antennas){
			ant.draw(g2d, mat);
		}
		
		DsrcVehicle dsrcVehicle;
		List<Information> infos = Collections.synchronizedList(new ArrayList<Information>());

		if (selectedVehicle != null) {
			if (!selectedVehicle.isDrawing()) {
				selectedVehicle = null;
				for (UpdateListener listener : SAVED_OBJECTS.getListeners(UpdateListener.class)) {
					listener.carClicked(false, "", false, false);
				}
			} else {
				PreCrashInformation crashInfo;
				if (selectedVehicle.isDsrc()) {
					dsrcVehicle = (DsrcVehicle) selectedVehicle;
					dsrcVehicle.drawInformationAccessZone(g2d, mat);
					dsrcVehicle.drawPropagationZone(g2d, mat);
					dsrcVehicle.drawDetectionZone(g2d, mat);
					infos = dsrcVehicle.getUpperLayer().getInformations();
					synchronized (infos) {
						for (Information info : infos) {
							if (info.getClass().equals(PreCrashInformation.class)) {
								crashInfo = (PreCrashInformation) info;
								crashInfo.drawCrashedZone(g2d, mat);
							}
						}
					}
				}
				selectedVehicle.drawSelected(g2d, mat);
			}
		}else if(selectedAntenna != null){
			selectedAntenna.drawInformationAccessZone(g2d, mat);
			selectedAntenna.drawPropagationZone(g2d, mat);
			selectedAntenna.drawSelected(g2d,mat);
		}

		g2d.drawString("" + time / 1000.0, 0, 10);

	}

	/**
	 * Dessine les routes sur la carte
	 * 
	 * @param g2d
	 *            environnement graphique
	 * @param mat
	 *            matrice de transformation (du mod�le physique)
	 */
	// auteur: Philippe
	private void drawEdges(Graphics2D g2d, AffineTransform mat) {
		Path2D.Double edges = new Path2D.Double();

		Color intialColor = g2d.getColor();
		g2d.setColor(Color.BLACK);

		// horizontals
		edges.moveTo(x1, y1);
		edges.lineTo(x9, y1);

		edges.moveTo(x2, y2);
		edges.lineTo(x3, y3);
		edges.lineTo(x7, y3);

		edges.moveTo(x2, y4);
		edges.lineTo(x8, y4);

		edges.moveTo(x1, y5);
		edges.lineTo(x7, y5);

		// verticals

		edges.moveTo(x2, y1);
		edges.lineTo(x2, y5);

		edges.moveTo(x3, y1);
		edges.lineTo(x3, y5);

		edges.moveTo(x4, y1);
		edges.lineTo(x4, y5);

		edges.moveTo(x6, y1);
		edges.lineTo(x5, y3);
		edges.lineTo(x5, y5);

		edges.moveTo(x8, y1);
		edges.lineTo(x7, y3);
		edges.lineTo(x7, y6);

		edges.moveTo(x8, y1);
		edges.lineTo(x8, y4);

		g2d.draw(mat.createTransformedShape(edges));
		g2d.setColor(intialColor);
	}

	/**
	 * Ajoute les obstacles � la liste d'obstacles
	 */
	// auteur: Philippe
	private void addObstacles() {
		int yU1 = y1 + 5, yD1 = y1 - 5, yU3 = y3 + 5, yD3 = y3 - 5, yU4 = y4 + 5, yD4 = y4 - 5, yU5 = y5 + 5, yD5 = y5 - 5;
		int xL2 = x2 - 5, xR2 = x2 + 5, xL3 = x3 - 5, xR3 = x3 + 5, xL4 = x4 - 5, xR4 = x4 + 5, xL5 = x5 - 5, xR5 = x5 + 5, xL6 = x6 - 5, xR6 = x6 + 5, xL7 = x7 - 5, xL8 = x8 - 5, xR8 = x8 + 5, xR9 = x9 + 5;
		int buildingDepth1 = 20, buildingDepth2 = 50, buildingDepth3 = 125;
		int buildingWidth1 = 30, buildingWidth2 = 50, buildingWidth3 = 100;
		int buildingGap = 12;

		// first Row
		obstacles.add(new ObstacleRect(xL2 - buildingWidth2, xL2, yU1, yU1 + buildingDepth2, showImages));
		obstacles.add(new ObstacleRect(xL2 - buildingWidth2 - buildingGap - buildingWidth1, xL2 - buildingWidth2 - buildingGap, yU1, yU1 + buildingDepth1, showImages));
		obstacles.add(new ObstacleRect(xL2 - buildingWidth2 - buildingWidth1 - buildingWidth3 - 2 * buildingGap, xL2 - buildingWidth2 - buildingWidth1 - 2 * buildingGap, yU1, yU1 + buildingDepth3, showImages));
		obstacles.add(new ObstacleRect(xR2, xR2 + buildingWidth3, yU1, yU1 + buildingDepth2, showImages));
		obstacles.add(new ObstacleRect(xR2 + buildingWidth3 + buildingGap, xL3, yU1, yU1 + buildingDepth3, showImages));
		obstacles.add(new ObstacleRect(xR3, xR3 + buildingWidth3, yU1, yU1 + buildingDepth2, showImages));
		obstacles.add(new ObstacleRect(xR4, xR4 + buildingWidth3, yU1, yU1 + buildingDepth3, showImages));
		obstacles.add(new ObstacleRect(xR4 + buildingWidth3 + buildingGap, xL6, yU1, yU1 + buildingDepth2, showImages));
		obstacles.add(new ObstacleRect(xR6, xR6 + buildingWidth3, yU1, yU1 + buildingDepth2, showImages));
		obstacles.add(new ObstacleRect(xR6 + buildingWidth3 + buildingGap, xR6 + buildingWidth3 + buildingGap + buildingWidth2, yU1, yU1 + buildingDepth1, showImages));
		obstacles.add(new ObstacleRect(xR6 + buildingWidth3 + buildingGap + buildingWidth2 + buildingGap, xR6 + buildingWidth3 + buildingGap + buildingWidth2 + buildingGap + buildingWidth1, yU1, yU1 + buildingDepth2, showImages));
		obstacles.add(new ObstacleRect(xR6 + buildingWidth3 + 3 * buildingGap + buildingWidth2 + buildingWidth1, xR6 + buildingWidth3 + 3 * buildingGap + buildingWidth2 + buildingWidth1 + buildingWidth3, yU1, yU1 + buildingDepth3, showImages));
		obstacles.add(new ObstacleRect(xR6 + 4 * buildingGap + buildingWidth1 + buildingWidth2 + 2 * buildingWidth3, xR6 + 4 * buildingGap + buildingWidth1 + 2 * buildingWidth2 + 2 * buildingWidth3, yU1, yU1 + buildingDepth2, showImages));
		obstacles.add(new ObstacleRect(xR6 + 5 * buildingGap + buildingWidth1 + 2 * buildingWidth2 + 2 * buildingWidth3, xR6 + 5 * buildingGap + buildingWidth1 + 2 * buildingWidth2 + 3 * buildingWidth3, yU1, yU1 + buildingDepth2, showImages));
		obstacles.add(new ObstacleRect(xR6 + 6 * buildingGap + buildingWidth1 + 2 * buildingWidth2 + 3 * buildingWidth3, xL8, yU1, yU1 + buildingDepth3, showImages));
		obstacles.add(new ObstacleRect(xR8, xR9, yU1, yU1 + buildingDepth3, showImages));

		// second Row
		obstacles.add(new ObstacleRect(x1, x1 + buildingWidth3, yD1 - buildingDepth3, yD1, showImages));
		obstacles.add(new ObstacleRect(x1 + buildingWidth3 + buildingGap, x1 + 2 * buildingWidth3 + buildingGap, yD1 - buildingDepth3, yD1, showImages));
		obstacles.add(new ObstacleRect(x1 + 2 * buildingWidth3 + 2 * buildingGap, x1 + 3 * buildingWidth3 + 2 * buildingGap, yD1 - buildingDepth3, yD1, showImages));
		obstacles.add(new ObstacleRect(x1 + 3 * buildingWidth3 + 3 * buildingGap, xL2, yD1 - buildingDepth3, yD1, showImages));
		obstacles.add(new ObstacleRect(xR2, xR2 + buildingWidth1, yD1 - buildingDepth2, yD1, showImages));
		obstacles.add(new ObstacleRect(xR2 + buildingWidth1 + buildingGap, xR2 + 2 * buildingWidth1 + buildingGap, yD1 - buildingDepth2, yD1, showImages));
		obstacles.add(new ObstacleRect(xR2 + 2 * buildingWidth1 + 2 * buildingGap, xR2 + 3 * buildingWidth1 + 2 * buildingGap, yD1 - buildingDepth2, yD1, showImages));
		obstacles.add(new ObstacleRect(xR2 + 3 * buildingWidth1 + 3 * buildingGap, xR2 + 4 * buildingWidth1 + 3 * buildingGap, yD1 - buildingDepth2, yD1, showImages));
		obstacles.add(new ObstacleRect(xR2 + 4 * buildingWidth1 + 4 * buildingGap, xR2 + 5 * buildingWidth1 + 4 * buildingGap, yD1 - buildingDepth2, yD1, showImages));
		obstacles.add(new ObstacleRect(xR2 + 5 * buildingWidth1 + 5 * buildingGap, xR2 + 6 * buildingWidth1 + 5 * buildingGap, yD1 - buildingDepth2, yD1, showImages));
		obstacles.add(new ObstacleRect(xR2 + 6 * buildingWidth1 + 6 * buildingGap, xL3, yD1 - buildingDepth2, yD1, showImages));
		obstacles.add(new ObstacleRect(xR3, xR3 + buildingWidth2, yD1 - buildingDepth2, yD1, showImages));
		obstacles.add(new ObstacleRect(xR3 + buildingWidth2 + buildingGap, xR3 + buildingGap + buildingWidth2 + buildingWidth3, yD1 - buildingDepth1, yD1, showImages));
		obstacles.add(new ObstacleRect(xR3 + 2 * buildingGap + buildingWidth2 + buildingWidth3, xL4, yD1 - buildingDepth2, yD1, showImages));
		obstacles.add(new ObstacleRect(xR4, xR4 + buildingWidth2, yD1 - buildingDepth1, yD1, showImages));
		obstacles.add(new ObstacleRect(xR6, xR6 + buildingWidth3, yD1 - buildingDepth1, yD1, showImages));
		obstacles.add(new ObstacleRect(xR8, xR9, yD1 - buildingDepth2, yD1, showImages));

		// third Row
		obstacles.add(new ObstacleRect(xL2 - buildingDepth1, xL2, yU3, yU3 + buildingWidth1, showImages));
		obstacles.add(new ObstacleRect(xR3, xL4, yU3, yU3 + buildingDepth2, showImages));
		obstacles.add(new ObstacleRect(xR4, xL5, yU3, yU3 + buildingDepth3, showImages));
		obstacles.add(new ObstacleRect(xR5 + buildingGap, xR5 + buildingGap + buildingWidth3, yU3, yU3 + buildingDepth1, showImages));
		obstacles.add(new ObstacleRect(xR5 + 2 * buildingGap + buildingWidth3, xR5 + 2 * buildingGap + buildingWidth3 + buildingWidth2, yU3, yU3 + buildingDepth2, showImages));
		obstacles.add(new ObstacleRect(xR5 + 3 * buildingGap + buildingWidth3 + buildingWidth2, xL7, yU3, yU3 + buildingDepth3, showImages));
		obstacles.add(new ObstacleRect(xR5 + buildingGap, xR5 + buildingGap + buildingWidth3, yU3, yU3 + buildingDepth1, showImages));
		obstacles.add(new ObstacleRect(xR5 + buildingGap + buildingGap + buildingWidth3, xR5 + buildingGap + buildingGap + buildingWidth3 + buildingWidth2, yU3, yU3 + buildingDepth2, showImages));
		obstacles.add(new ObstacleRect(xR5 + buildingGap + 2 * buildingGap + buildingWidth3 + buildingWidth2, xL7, yU3, yU3 + buildingDepth3, showImages));

		// fourth row
		obstacles.add(new ObstacleRect(xR2, xR2 + buildingWidth1, yU4, yU4 + buildingDepth1, showImages));
		obstacles.add(new ObstacleRect(xR4, xR4 + buildingDepth3, yU4, yU4 + buildingWidth2, showImages));
		obstacles.add(new ObstacleRect(xR4, xR4 + buildingWidth3, yD3 - buildingDepth2, yD3, showImages));
		obstacles.add(new ObstacleRect(xR4, xR4 + buildingDepth2, yU4 + buildingWidth2 + buildingGap, yD3 - buildingGap - buildingDepth2, showImages));

		// fifth row
		obstacles.add(new ObstacleRect(xL3 - buildingWidth3, xL3, yU5, yU5 + buildingDepth2, showImages));
		obstacles.add(new ObstacleRect(xR5, xR5 + buildingWidth3, yD4 - buildingDepth2, yD4, showImages));
		obstacles.add(new ObstacleRect(xR5 + buildingWidth3 + buildingGap, xL7, yD4 - buildingDepth3, yD4, showImages));

		// sixth row
		obstacles.add(new ObstacleRect(xR4 - (buildingWidth3 / 2), xR4 + (buildingWidth3 / 2), yD5 - buildingDepth3, yD5, showImages));
	}

	// auteur : Cedryk
	/**
	 * Roule l'animation
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		while (!traciConnection.isClosed() && !events.isEmpty() && animationRunning) {

			nextStep(false);
			try {
				Thread.sleep(Math.max(updateFrequency - pastTime, 0));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		animationRunning = false;
	}

	/**
	 * Passe � la prochaine �tape dans la simulation
	 * 
	 * @param stepByStep
	 *            vrai si l'application est en mode pas a pas
	 * 
	 * @return l'�v�nement courant
	 */
	// auteur : cedryk
	public Event nextStep(boolean stepByStep) {
		long initialTime = System.currentTimeMillis();

		Event event = events.remove();
		event.run();

		time = traciConnection.getCurrentSimTime();

		repaint();
		if (event.getClass().equals(Update.class)) {
			for (UpdateListener listener : SAVED_OBJECTS.getListeners(UpdateListener.class)) {
				if (selectedVehicle != null && (stepByStep || increment % infoUpdateFrequency == 0)) {
					selectedVehicle.carInformationUpdated(listener);
					if (selectedVehicle.isDsrc()) {
						updateChartsValues((DsrcVehicle) selectedVehicle, listener, false);
					}
				}
				ArrayList<DsrcVehicle> dsrcVehicles = new ArrayList<DsrcVehicle>();
				for(ScvVehicle vehicle: vehicles){
					if(vehicle.isDsrc()){
						dsrcVehicles.add((DsrcVehicle)vehicle);
					}
				}
				
				listener.simulationUpdated(dsrcVehicles);
			}
			increment++;
			pastTime = System.currentTimeMillis() - initialTime;
		} else {
			pastTime = updateFrequency;
		}

		deleteDecayedInfo();
		validateInfos();
		return event;
	}

	private void validateInfos() {
		for(ScvVehicle vehicle: vehicles){
			if(!vehicle.isDsrc())
				continue;
			
			String sumoId = vehicle.getSumoVehicle().getID();
			DsrcVehicle dsrcVehicle = (DsrcVehicle) vehicle;
			Connection connect = connectionManager.findConnection(dsrcVehicle.getPhysicLayer());
			ArrayList<CarPosition> carPositionInfos = new ArrayList<CarPosition>();
			for(PhysicLayer layer: connect.getReceivers()){
				List<Information> infos = layer.getUpperLayer().getInformations();
				for(Information info: infos){
					if(info.getId().startsWith(sumoId) && info.getId().endsWith("1"))
						carPositionInfos.add((CarPosition)info);
				}
			}
			int nbErrors = 0;
			ArrayList<CarPosition> carPositionCopy = (ArrayList<CarPosition>) carPositionInfos.clone();
			for(CarPosition pos: carPositionInfos){
				for(CarPosition posToCompare: carPositionCopy){
					double x1 = Math.abs(pos.getPosition().getX());
					double y1 = Math.abs(pos.getPosition().getY());
					double x2 = Math.abs(posToCompare.getPosition().getX());
					double y2 = Math.abs(posToCompare.getPosition().getY());
					if((x1 < x2 - POSITION_ACCURACY*x2 || x1 > x2 + POSITION_ACCURACY*x2 || y1 < y2 - POSITION_ACCURACY*y2 || y1 > y2 + POSITION_ACCURACY*y2) && pos.getDecayTime() == posToCompare.getDecayTime()){
						nbErrors++;
					}
				}
			}
			/*for(CarPosition pos: carPositionInfos){
				int x1 = (int) Math.abs(Math.round(pos.getPosition().getX()));
				int y1 = (int) Math.abs(Math.round(pos.getPosition().getY()));
				int x2 = (int)Math.abs(Math.round(vehicle.getPosition().getX()));
				int y2 = (int)Math.abs(Math.round(vehicle.getPosition().getY()));
				if(x1 < x2 - 5 || x1 > x2 + 5 || y1 < y2 - 5 || y1 > y2 + 5){
					nbErrors++;
				}
			}*/
			VehicleTrustPoints.adjustPoints(sumoId, nbErrors, time);
		}
	}

	/**
	 * Passe � la prochaine etape tant qu'elle n'est pas un update
	 */
	// auteur: cedryk
	public void stepByStep() {
		Event event;
		do {
			event = nextStep(true);
		} while (!event.getClass().equals(Update.class));
	}

	/**
	 * Enleve les infos 'passees date' de chaque vehicule
	 */
	// auteur : philippe
	private void deleteDecayedInfo() {
		ArrayList<Information> infoToDelete;
		// synchronized (vehicles) {
		for (ScvVehicle vehicle : vehicles) {
			if (vehicle.isDsrc()) {
				DsrcVehicle dsrcVehicle = (DsrcVehicle) vehicle;
				infoToDelete = new ArrayList<Information>();
				for (Information info : dsrcVehicle.getUpperLayer().getInformations()) {

					if (info.getDecayTime() < time) {
						infoToDelete.add(info);
					}
				}
				dsrcVehicle.getUpperLayer().getInformations().removeAll(infoToDelete);
			}
		}
		// }
	}

	/**
	 * D�marre la connexion si elle n'est pas d�j� d�marr�e et d�marre ou
	 * poursuit l'animation. Si animate est false, l'animation ne d�marre pas
	 * imm�diatement
	 * 
	 * @param animate
	 *            bool�en pour contr�ler si l'animation d�marre tout de suite
	 */
	// auteur : cedryk
	public void start(boolean animate) {
		if (proc == null) {
			try {
				traciConnection.runServer();
				connectionUp = true;
			} catch (InterruptedException | IOException e) {
				System.out.println(e.getMessage());
			}
		}
		if (!animationRunning) {
			proc = new Thread(this);
			if (animate) {
				animationRunning = true;
				proc.start();
			}
		}

	}

	/**
	 * Retourne la liste de v�hicules
	 * 
	 * @return la liste de v�hicules
	 */
	// auteur : philippe
	public static List<ScvVehicle> getVehicles() {
		return vehicles;
	}

	/**
	 * Ferme les communications avec Traci et stoppe l'animation
	 */
	// auteur : cedryk
	public void closeSimulation() {
		try {
			animationRunning = false;
			Thread.sleep(2 * updateFrequency);
			traciConnection.close();
			connectionUp = false;
			Update.resetCurrDsrcVehicles();
		} catch (IOException | InterruptedException e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Retourne la liste d'obstacles
	 * 
	 * @return la liste d'obstacles
	 */
	// auteur : philippe
	public static ArrayList<Obstacle> getObstacles() {
		return obstacles;
	}

	/**
	 * Retourne le connection manager
	 * 
	 * @return le connection manager
	 */
	// auteur : philippe
	public static ConnectionManager getConnectionManager() {
		return connectionManager;
	}

	/**
	 * Retourne l'objet de communication avec traci
	 * 
	 * @return l'objet de communication avec traci
	 */
	// auteur : philippe
	public static SumoTraciConnection getTraciConnection() {
		return traciConnection;
	}

	/**
	 * Arr�te la boucle d'animation pour la mettre en pause
	 */
	// auteur : cedryk
	public void pauseSimulation() {
		animationRunning = false;
	}

	/**
	 * Retourne le temps de la simulation
	 * 
	 * @return le temps de la simulation
	 */
	// auteur : philippe
	public static int getTime() {
		return time;
	}

	/**
	 * Retourne true si l'animation roule et false sinon
	 * 
	 * @return le bool�en animationRunning
	 */
	// auteur : cedryk
	public boolean animationIsRunning() {
		return animationRunning;
	}

	/**
	 * Retourne true si la connection avec traci est instanci�e et false sinon
	 * 
	 * @return le bool�en connectionUp
	 */
	// auteur : cedryk
	public boolean isConnectionUp() {
		return connectionUp;
	}

	/**
	 * Change la vitesse d'animation
	 * 
	 * @param speed
	 *            facteur multiplicateur de la vitesse d'animation
	 */
	// auteur : cedryk
	public void changeAnimationSpeed(int speed) {
		animationSpeed = speed;
		stepLength = (long) (updateFrequency * animationSpeed);
		if (traciConnection != null) {
			traciConnection.setStepLength((int) stepLength);
			Update.setStepLength((int) stepLength);
		}
	}

	// auteur : cedryk
	/**
	 * Instancie tous les champs de l'objet ScvAnimation
	 * 
	 * @param speed
	 *            la vitesse initiale d'animation
	 * @param fileName
	 *            le nom du fichier de configuration de Sumo
	 */
	public void initialize(int speed, String fileName) {
		traciConnection = new SumoTraciConnection("sumoXml/" + fileName, 12345);
		vehicles = Collections.synchronizedList(new ArrayList<ScvVehicle>());
		obstacles = new ArrayList<Obstacle>();
		antennas = Collections.synchronizedList(new ArrayList<Antenna>());
		connectionManager = new ConnectionManager();
		time = 0;
		updateFrequency = 16;
		changeAnimationSpeed(speed);
		events = new EventList();
		events.addEvent(new Update(0));
		proc = null;
		connectionUp = false;
		selectedVehicle = null;
		selectedAntenna = null;
		addObstacles();
		firstTime = true;
		zoom = 0;
		Update.setStepLength((int) stepLength);
		Antenna.resetIdNUmber();
		VehicleTrustPoints.initializeTrustPoints();
		repaint();
	}

	/**
	 * cause un accident au vehicule selectionne
	 */
	public void crash() {
		if (selectedVehicle.isCrashed()) {
			selectedVehicle.setCrashed(false);
			try {
				selectedVehicle.getSumoVehicle().changeSpeed((double) ScvVehicle.getMaxSpeed());
				System.out.println("Le vehicule \""+selectedVehicle+"\" a ete repare");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			selectedVehicle.setCrashed(true);
			try {
				selectedVehicle.getSumoVehicle().changeSpeed((double) 0);
				System.out.println("Le vehicule \""+selectedVehicle+"\" a eu un accident");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		for (UpdateListener listener : SAVED_OBJECTS.getListeners(UpdateListener.class)) {
			listener.carIsCrashed(selectedVehicle.isCrashed());
		}
	}

	/**
	 * Ajoute un evenement a la liste d'evenements
	 * 
	 * @param eventToAdd
	 *            evenement a ajouter
	 */
	// auteur : cedryk
	public static void addEvent(Event eventToAdd) {
		events.addEvent(eventToAdd);
	}

	/**
	 * Change la frequence de mise a jour de la simulation
	 * 
	 * @param updateFrequency
	 *            le temps entre chaque mise a jour, en ms
	 */
	// auteur : cedryk
	public void setUpdateFrequency(int updateFrequency) {
		this.updateFrequency = updateFrequency;
		changeAnimationSpeed(animationSpeed);
		Update.setStepLength((int) stepLength);
	}

	/**
	 * Ajoute les objets � la liste d'objets qui �coutent les �v�nements
	 * 
	 * @param listeningObj
	 *            l'objet qui veut �couter
	 */
	// auteur : cedryk
	public void addUpdateListener(UpdateListener listeningObj) {
		SAVED_OBJECTS.add(UpdateListener.class, listeningObj);
	}

	/**
	 * Change la frequence de mise a jour des informations de la voiture
	 * selectionnee
	 * 
	 * @param infoUpdateFrequency
	 *            le nombre de ticks entre chaque rafraichissement
	 */
	// auteur : cedryk
	public void setInfoUpdateFrequency(int infoUpdateFrequency) {
		this.infoUpdateFrequency = infoUpdateFrequency;
	}

	/**
	 * met a jour les graphiques
	 * 
	 * @param vehicle
	 *            le vehicule dont le graphique est mis a jour
	 * @param listener
	 *            l'ecouteur de mise a jour
	 * @param carChanged
	 *            si le vehicule selectionne a change ou non
	 */
	private void updateChartsValues(DsrcVehicle vehicle, UpdateListener listener, boolean carChanged) {
		listener.chartsValuesUpdated(carChanged, vehicle.getDirectConnectionTriedStats(), vehicle.getDirectConnectionsStats(), vehicle.getDirectConnectionsFailedObstacleStats(), vehicle.getDirectConnectionsFailedCollisionStats(), vehicle.getDirectConnectionSuceededRate(), vehicle.getNoiseStats());
	}

	/**
	 * Met l'animation en mode ajout d'antenne
	 */
	public void addAntenna(boolean adding) {
		placingInfra = adding;
	}

	public static List<Antenna> getAntennas() {
		return antennas;
	}

	public void removeSelectedAntenna() {
		if(selectedAntenna != null){
			antennas.remove(selectedAntenna);
			selectedAntenna = null;
			for (UpdateListener listener : SAVED_OBJECTS.getListeners(UpdateListener.class)) {
				listener.antennaClicked(false, null, "");
			}
			repaint();
		}
	}
}
