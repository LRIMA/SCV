package charts;

import java.awt.geom.Point2D;
import java.util.ArrayList;

/**
 * Classe modelisant une statistique a analyser
 * @author cedryk
 *
 */
public class Statistic {
	
	private ArrayList<Point2D.Double> values;
	private String name;
	private Units yUnits;
	private Units xUnits;
	
	/**
	 * cree une statistique en specifiant son nom
	 * @param name le nom de la statistique
	 * @param yUnits les unites en Y
	 */
	public Statistic(String name, Units yUnits){
		this.name = name;
		this.values = new ArrayList<Point2D.Double>();
		this.yUnits = yUnits;
		xUnits = Units.TIME;
	}
	
	/**
	 * retourne le nom dela statistique
	 * @return le nom
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * retourne les valeurs associ�es a la statistique
	 * @return les valeurs en liste de points
	 */
	public ArrayList<Point2D.Double> getValues(){
		return values;
	}
	
	/**
	 * change les valeurs associ�es a la statistique
	 * @param values les valeurs a changer
	 */
	public void setValues(ArrayList<Point2D.Double> values){
		this.values = values;
	}
	
	/**
	 * ajoutes une valeur aux valeurs existantes
	 * @param value valeur a ajouter (point)
	 * @return true si la valeur a pu etre ajoutee, false sinon
	 */
	public boolean addValue(Point2D.Double value){
		return values.add(value);
	}
	
	/**
	 * Retourne les unit�s en Y des valeurs
	 * @return les unit�s en Y
	 */
	public Units getYUnits(){
		return yUnits;
	}
	
	/**
	 * Retrourne les unit�s en X des valeurs
	 * @return les unit�s en X
	 */
	public Units getXUnits(){
		return xUnits;
	}
	
	public boolean equals(Object other){
		if(other != null && this != null && other.getClass() == this.getClass()){
			Statistic otherStat = (Statistic) other;
			if(this.getName().equals(otherStat.getName())){
				return true;
			}
		}
		return false;
	}
	
	public String toString(){
		return name;
	}

}
