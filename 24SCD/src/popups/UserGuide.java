package popups;

import java.awt.Container;

import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;

import aapplication.AnimationFrame;
import javax.swing.JTextPane;

/**
 * Une fenetre explicative contenant un guide d'utilisateur pourles deux pages de la fenetre principale de l'application
 * @author _Cedryk_
 *
 */
public class UserGuide extends Popup {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8154125012556213752L;
	private JTabbedPane contentPane;
	private JScrollPane launchFrameGuide;
	private JScrollPane animationFrameGuide;
	private JTextPane txtpnlLaunchFrameGuide;
	private JTextPane txtpnlAnimationFrameGuide;

	/**
	 * Create the frame.
	 * 
	 * @param classApp24ContentPane la classe du contentPane de App24SCv
	 */
	public UserGuide(Class<? extends Container> classApp24ContentPane) {
		super();
		setTitle("Guide d'utilisateur");
		contentPane = new JTabbedPane();
		setContentPane(contentPane);
		
		launchFrameGuide = new JScrollPane();
		contentPane.addTab("Guide de la fen\u00EAtre de lancement", null, launchFrameGuide, null);
		
		txtpnlLaunchFrameGuide = new JTextPane();
		txtpnlLaunchFrameGuide.setText("Guide d'utilisation de la fen\u00EAtre de lancement : \r\n\r\nDans cette fen\u00EAtre, vous pouvez choisir les param\u00E8tres initiaux de la simulation. Ces param\u00E8tres influeront sur plusieurs aspects de l'application, soit sur le fonctionnement du simulateur ou encore sur l'affichage des r\u00E9sultats.\r\n\r\nVoici les param\u00E8tres et leurs effets : \r\n\r\nOPTIONS G\u00C9N\u00C9RALES : \r\n\t\r\n1. Fr\u00E9quence de mise \u00E0 jour des informations d'une voiture s\u00E9lectionn\u00E9e (1 \u00E0 20 ticks)\r\nCe param\u00E8tre change la vitesse \u00E0 laquelle les informations d'une voiture s\u00E9lectionn\u00E9e se rafra\u00EEchissent. Une valeur plus \u00E9lev\u00E9e signifie un rafra\u00EEchissement plus lent. Par d\u00E9faut \u00E0 5ms.\r\n\r\nOPTIONS DE SIMULATION :\r\n\r\n1. Fr\u00E9quence de mise \u00E0 jour (16ms \u00E0 100ms)\r\nCe param\u00E8tre change le temps \u00E9coul\u00E9 entre chaque image. 16ms signifie que 60 images sont affich\u00E9es par seconde alors que 100ms signifie que seulement 10 sont affich\u00E9es. Il est important de noter que la quantit\u00E9 d'images affich\u00E9e d\u00E9pend aussi de la rapidit\u00E9 de l'ordinateur et de la complexit\u00E9 de la simulation. Par d\u00E9faut \u00E0 16ms.\r\n\r\n2. Temps de vie des messages (500ms \u00E0 2000ms)\r\nCeci change le temps avant qu'un message soit consid\u00E9r\u00E9 comme plus valide. Une valeur plus \u00E9lev\u00E9e peut r\u00E9duire la vitesse d'animation. Par d\u00E9faut \u00E0 1000ms.\r\n\r\n3. Fr\u00E9quence d'envoi des messages p\u00E9riodiques (50ms \u00E0 500ms)\r\nCe param\u00E8tre change le temps entre chaque envoi de messages p\u00E9riodiques par les v\u00E9hicules avec la technologie DSRC. Par d\u00E9faut \u00E0 100ms.\r\n\r\n4. Sensibilit\u00E9 de la radio (0 \u00E0 1.2 W)\r\nCe param\u00E8tre change la sensibilit\u00E9 de la radio des v\u00E9hicules. Une valeur plus \u00E9lev\u00E9e signifie que les v\u00E9hicules vont avoir plus de difficult\u00E9 \u00E0 se communiquer des informations, mais qu'il y aura moins d'interf\u00E9rence par les autres v\u00E9hicules. Par d\u00E9faut \u00E0 0.8 W\r\n\r\n5. Pourcentage de v\u00E9hicules avec la technologie DSRC\r\nCe param\u00E8tre change le pourcentage de v\u00E9hicules dans la simulation poss\u00E9dant la technologie DSRC. Une plus haute valeur risque de r\u00E9duire la vitesse d'animation. Par d\u00E9faut \u00E0 50%.\r\n\r\nOPTIONS D'AFFICHAGE :\r\n\r\n1. Images pour les v\u00E9hicules\r\nAfficher les v\u00E9hicules avec une image ou avec un cercle de couleur. Par d\u00E9faut \u00E0 images.\r\n\r\n2.Images pour les obstacles\r\nAfficher les obstacles avec une image ou avec un rectangle de couleur. Par d\u00E9faut \u00E0 images.\r\n\r\nCHOIX D'UNE ROUTE :\r\n\r\nIl est possible de charger une route dans la simulation diff\u00E9rente de la route par d\u00E9faut. Pour ce faire, il sufft d'appuyer sur le bouton Ouvrir, naviguer jusqu'au fichier souhait\u00E9 et le s\u00E9lectionner puis appuyer sur Open. Ensuite, il ne reste qu'\u00E0 s'assurer que le fichier se charge correctement en v\u00E9rifiant son contenu dans la zone de texte puis de confirmer le choix \u00E0 l'aide du bouton Confirmer. Le fichier sera ensuite charg\u00E9 dans l'application.");
		txtpnlLaunchFrameGuide.setCaretPosition(0);
		txtpnlLaunchFrameGuide.setEditable(false);
		launchFrameGuide.setViewportView(txtpnlLaunchFrameGuide);
		animationFrameGuide = new JScrollPane();
		contentPane.addTab("Guide de la fen\u00EAtre d'animation", null, animationFrameGuide, null);
		
		txtpnlAnimationFrameGuide = new JTextPane();
		txtpnlAnimationFrameGuide.setEditable(false);
		txtpnlAnimationFrameGuide.setText("Guide d'utilisation de la fen\u00EAtre d'animation : \r\n\r\nDans cette fen\u00EAtre, vous pourrez observer la simulation anim\u00E9e. Pour d\u00E9marrer l'animation, appuyez sur le bouton D\u00E9marrer ou Pas \u00E0 pas. Voici l'explication des boutons et autres entr\u00E9es de cette fen\u00EAtre : \r\n\r\n1. bouton D\u00E9marrer/Pause :\r\nCe bouton sert \u00E0 d\u00E9marrer, mettre en pause ou poursuivre l'animation. Apr\u00E8s avoir appuy\u00E9 une fois dessus, le bouton change pour le bouton pause et l'animation d\u00E9marre. Si vous r\u00E9appuyez, il redevient le bouton D\u00E9marrer et l'animation se met en pause.\r\n\r\n2. bouton Pas \u00E0 pas : \r\nCe bouton sert \u00E0 parcourir l'animation un pas \u00E0 la fois. Si l'animation \u00E9tait en pause ou n'\u00E9tait pas d\u00E9marr\u00E9e, elle ne fait que passer \u00E0 la prochaine \u00E9tape. Si l'animation \u00E9tait en cours lors de l'appui de bouton, elle passe \u00E0 la prochaine \u00E9tape et se met en pause.\r\n\r\n3. glissi\u00E8re Vitesse d'animation : \r\nCette glissi\u00E8re permet d'ajuster la vitesse de l'animation. Une valeur plus \u00E9lev\u00E9e augmente le temps \u00E9coul\u00E9 dans la simulation entre chaque pas, mais pas le temps r\u00E9el. Une valeur de 1X signifierait qu'une seconde dans la vraie vie \u00E9quivaut \u00E0 une seconde dans la simulation, mais en r\u00E9alit\u00E9 ce n'est pas le cas puisque la vitesse d'animation d\u00E9pend de la rapidit\u00E9 de l'ordinateur et de la complexit\u00E9 de la simulation.\r\n\r\n4. bouton R\u00E9initialiser :\r\nCe bouton remet la vue de la carte comme elle \u00E9tait au d\u00E9part et remets l'animation au d\u00E9but, comme lorsque la fen\u00EAtre a \u00E9t\u00E9 ouverte pour la premi\u00E8re fois.\r\n\r\n5. bouton Retourner \u00E0 la page de lancement :\r\nCe bouton arr\u00EAte l'animation et renvoie l'utilisateur \u00E0 la page de lancement, o\u00F9 il peut choisir les valeurs de param\u00E8tres qu'il veut.\r\n\r\n6. bouton Ouvrir la page des graphiques\r\nCe bouton ouvre la page des graphiques, qui montre des informations int\u00E9ressantes sur le v\u00E9hicule s\u00E9lectionn\u00E9.\r\n\r\nDans la partie du bas, une console affiche des messages int\u00E9ressants pour vous ainsi que le d\u00E9veloppeur (peut \u00EAtre la m\u00EAme personne!)\r\n\r\nEnsuite, vous pouvez, dans la section de l'animation, zoomer \u00E0 l'aide de la molette de la souris ainsi que d\u00E9placer la carte en en effectuant un cliqu\u00E9-gliss\u00E9.\r\n\r\nFinalement, vous pouvez s\u00E9lectionner un v\u00E9hicule dans la zone d'animation en cliquant dessus et voir des informations utiles sur ce v\u00E9hicule dans le panneau de gauche. On peut y voir son nom, s'il utilise la technologie DSRC, sa position et sa vitesse. Cliquer \u00E0 nouveau sur le m\u00EAme v\u00E9hicule le d\u00E9selectionne et cliquer sur un autre v\u00E9hicule le s\u00E9lectionne. Lorsqu'un v\u00E9hicule avec la technologie DSRC est s\u00E9lectionn\u00E9, on peut voir dans la fen\u00EAtre d'animation sa zone de d\u00E9tection (en bleu), sa zone de propagation des ondes (en jaune) et sa zone d'acc\u00E8s \u00E0 l'informaiton(en rose).");
		txtpnlAnimationFrameGuide.setCaretPosition(0);
		animationFrameGuide.setViewportView(txtpnlAnimationFrameGuide);
		
		
		if(classApp24ContentPane.equals(AnimationFrame.class)){
			contentPane.setSelectedComponent(animationFrameGuide);
		}
	}

}
