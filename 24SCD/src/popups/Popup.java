package popups;

import java.awt.Toolkit;

import javax.swing.JFrame;

/**
 * classe abstraite regroupant les fenetres explicatives de l'application
 * @author _Philippe_
 *
 */
public abstract class Popup extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9188099269020394591L;
	private final int WIDTH = 700;
	private final int HEIGHT = 550;

	/**
	 * cree un Popup
	 */
	protected Popup() {
		setResizable(false);
		setAlwaysOnTop(true);
		setBounds((int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth() - WIDTH) / 2, (int) (Toolkit
				.getDefaultToolkit().getScreenSize().getHeight() - HEIGHT) / 2, WIDTH, HEIGHT);
		setVisible(true);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

}
