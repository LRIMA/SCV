package drawing;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;

/**
 * Classe contenant des methodes pour dessiner des aires translucides
 * @author _Philippe_
 *
 */
public class AreaDrawing {
	
	/**
	 * dessine une aire translucide
	 * 
	 * @param g2d
	 *            le Graphics2D qui dessinera le vehicule
	 * @param mat
	 *            la matrice de transformation servant a dessiner le vehicule
	 * @param color
	 *            la couleur de la zone a dessiner
	 * @param toPaint
	 *            la zone a dessiner
	 */
	public static void drawArea(Graphics2D g2d, AffineTransform mat, Color color, Area toPaint) {

		Color initialColor = g2d.getColor();

		g2d.setPaint(color);
		g2d.fill(mat.createTransformedShape(toPaint));

		g2d.setColor(initialColor);
	}
}
