package vehicle;

import java.awt.geom.Point2D;

/**
 * La couche physique du vehicule est utilisee pour calculer la connection avec
 * un autre vehicule
 * 
 * @author _Philippe_
 *
 */
public class PhysicLayer {

	private Point2D.Double position;
	private UpperLayer upperLayer;
	private static double sensitivity;
	private double noise;
	private VehicleRadioState state;
	private final String OBJECT_TYPE;

	/**
	 * cree un PhysicLayer
	 * 
	 * @param position
	 *            la position du vehicule
	 * @param upperLayer
	 *            le UpperLayer du vehicule
	 */
	public PhysicLayer(Point2D.Double position, UpperLayer upperLayer, String type) {
		this.position = position;
		this.upperLayer = upperLayer;
		noise = 0;
		state = VehicleRadioState.WAITING;
		OBJECT_TYPE = type;
	}

	/**
	 * retourne la position du vehicule
	 * 
	 * @return la position du vehicule
	 */
	public Point2D.Double getPosition() {
		return position;
	}
	
	/**
	 * defini la sensibilite des couches physiques
	 * @param _sensitivity la sensibilite a changer
	 */
	public static void setSensitivity(double _sensitivity){
		sensitivity= _sensitivity;
	}

	/**
	 * defini la position de la couche physique
	 * @param position la position du vehicule
	 */
	public void setPosition(Point2D.Double position) {
		this.position = position;
	}

	/**
	 * retourne le UpperLayer
	 * 
	 * @return le UpperLayer
	 */
	public UpperLayer getUpperLayer() {
		return upperLayer;
	}

	/**
	 * 
	 * @param messagePower
	 *            la puissance du message recu
	 * @return 0 si la communication est assez forte, le bruit si �a ne d�passe
	 *         pas la sensibilit�
	 */
	public double canReadNoisePossibility(double messagePower) {
		if (state == VehicleRadioState.WAITING) {
			if (messagePower >= sensitivity) {
				state = VehicleRadioState.LISTENING;
				return 0;
			} else {
				noise += messagePower;
				testNoiseListening();
				return messagePower;
			}
		} else {
			return -1;
		}
	}

	/**
	 * retourne le bruit percu par la couche physique
	 * @return le bruit percu par la couche physique
	 */
	public double getNoise() {
		return noise;
	}

	/**
	 * retire du bruit percu par la couche physique
	 * @param noiseToRemove la quantite de bruit a retirer
	 */
	public void removeNoise(double noiseToRemove) {
		noise -= noiseToRemove;
		testNoiseListening();
	}

	/**
	 * verifie si le bruit entendu par le vehicule suffit a le mettre en mode ecoute
	 */
	private void testNoiseListening() {
		if (noise >= sensitivity) {
			state = VehicleRadioState.LISTENING;
		} else {
			state = VehicleRadioState.WAITING;
		}
	}

	/**
	 * verifie si le message est assez puissant pour etre entendu
	 * @param messagePower la puissance du message
	 * @return vrai si le message est assez puissant, faux sinon
	 */
	public boolean canRead(double messagePower) {
		return messagePower >= sensitivity;
	}

	/**
	 * retourne le mode de la radio de la couche physique
	 * @return le mode de la radio de la couche physique
	 */
	public VehicleRadioState getState() {
		return state;
	}

	/**
	 * defini le mode de la couche physique 
	 * @param state le mode de la couche physique 
	 */
	public void setState(VehicleRadioState state) {
		this.state = state;
	}
	
	public String getType(){
		return OBJECT_TYPE;
	}

}
