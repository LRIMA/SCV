package vehicle;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.util.Enumeration;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;

import aapplication.ScvAnimation;
import charts.Statistic;
import charts.Units;
import drawing.AreaDrawing;
import event.DsrcWarning;
import information.CarPosition;
import information.PreCrashInformation;
import infrastructure.VehicleTrustPoints;
import it.polito.appeal.traci.Vehicle;
import listener.UpdateListener;
import obstacle.Obstacle;

/**
 * Classe decrivant un vehicule possedant des emetteurs et recepteurs DSRC ainsi
 * que des senseurs
 * 
 * @author _Philippe_
 *
 */
public class DsrcVehicle extends ScvVehicle {

	private static final int SHORT_RANGE = 30;
	private static final int CAMERA_RANGE = 200;
	private static final int BACK_RADAR_RANGE = 50;
	private static final int PROPAGATION_RANGE = 350;

	private UpperLayer upperLayer;
	private PhysicLayer physicLayer;
	private Area detectionZone;
	private static final Color TRANSPARENT_BLUE = new Color((float) (Color.BLUE.getRed() / 255.0), (float) (Color.BLUE.getGreen() / 255.0), (float) (Color.BLUE.getBlue() / 255.0), (float) 0.3),
			TRANSPARENT_ORANGE = new Color((float) (Color.ORANGE.getRed() / 255.0), (float) (Color.ORANGE.getGreen() / 255.0), (float) (Color.ORANGE.getBlue() / 255.0), (float) 0.3);

	private Statistic directConnectionSuccededStats;
	private Statistic directConnectionTriedStats;
	

	private Statistic directFailedConnectionsObstaclesStats;
	private Statistic directFailedConnectionsCollisionStats;
	
	private Statistic directConnectionSucceededRate;
	private Statistic noiseStats;

	private boolean malicious;
	/**
	 * cree un DsrcVehicle
	 * 
	 * @param position
	 *            position du vehicule
	 * @param sumoVehicle
	 *            vehicule de SUMO equivalent au vehicule SCV
	 * @param time le temps lors de la creation du vehicle
	 */
	public DsrcVehicle(Point2D.Double position, Vehicle sumoVehicle, double time, boolean malicious) {
		super(position, sumoVehicle);
		this.upperLayer = new UpperLayer(getSumoVehicle().getID());
		this.physicLayer = new PhysicLayer(position, upperLayer, "vehicle");
		calculateDetectionZone();
		directConnectionSuccededStats = new Statistic("Connections directes reussies", Units.CONNECTIONS);
		
		directFailedConnectionsObstaclesStats = new Statistic("Connections directes echouees par les obstacles", Units.CONNECTIONS);
		
		directFailedConnectionsCollisionStats = new Statistic("Connections directes echouees par collision des paquets", Units.CONNECTIONS);
		
		directConnectionTriedStats = new Statistic("Connections directes essayees", Units.CONNECTIONS);
		
		directConnectionSucceededRate = new Statistic("Taux de fiabilité des communications", Units.PERCENTAGE);
		
		noiseStats = new Statistic("Bruit de la radio", Units.POWER);
		
		this.malicious = malicious;
	}

	/**
	 * Calcule la zone de detection du vehicule Dsrc. Celle-ci est limitee par
	 * la portee des senseurs et par les obstacles
	 */
	public void calculateDetectionZone() {
		Area detectionZoneTemp = new Area();
		shortRangeRadar(detectionZoneTemp);
		camera(detectionZoneTemp);
		backRadar(detectionZoneTemp);

		obstacleReduceDetection(detectionZoneTemp);

		this.detectionZone = detectionZoneTemp;

		addCarPostionInformation();
		upperLayer.setDetectionZone(detectionZone);
	}

	/**
	 * ajoute la zone du radar arriere a la zone de detection du vehicule
	 * @param detectionZoneTemp la zone de detection temporaire du vehicule
	 */
	private void backRadar(Area detectionZoneTemp) {
		Area toAdd;
		double openingAngle = 30;
		double reversedOrientation = getOrientation() + Math.PI;

		Ellipse2D.Double camera = new Ellipse2D.Double(getPosition().getX() - BACK_RADAR_RANGE, getPosition().getY() - BACK_RADAR_RANGE, BACK_RADAR_RANGE * 2, BACK_RADAR_RANGE * 2);
		toAdd = new Area(camera);

		Path2D.Double angleRestriction = new Path2D.Double();
		angleRestriction.moveTo(getPosition().getX(), getPosition().getY());
		angleRestriction.lineTo(getPosition().getX() + Math.cos(reversedOrientation + Math.toRadians(openingAngle / 2)) * 1.5 * BACK_RADAR_RANGE, getPosition().getY() + Math.sin(-reversedOrientation - Math.toRadians(openingAngle / 2)) * 1.5 * BACK_RADAR_RANGE);
		angleRestriction.lineTo(getPosition().getX() + Math.cos(reversedOrientation - Math.toRadians(openingAngle / 2)) * 1.5 * BACK_RADAR_RANGE, getPosition().getY() + Math.sin(-reversedOrientation + Math.toRadians(openingAngle / 2)) * 1.5 * BACK_RADAR_RANGE);
		angleRestriction.closePath();
		toAdd.intersect(new Area(angleRestriction));

		detectionZoneTemp.add(toAdd);

	}

	/**
	 * ajoute le radar a courte portee a la zone de detection du vehicule
	 * @param detectionZoneTemp la zone de detection temporaire
	 */
	private void shortRangeRadar(Area detectionZoneTemp) {
		Ellipse2D.Double shortRangeRadar = new Ellipse2D.Double(getPosition().getX() - SHORT_RANGE, getPosition().getY() - SHORT_RANGE, SHORT_RANGE * 2, SHORT_RANGE * 2);
		detectionZoneTemp.add(new Area(shortRangeRadar));
	}

	/**
	 * ajoute la camera avant a la zone de detection du vehicule
	 * @param detectionZoneTemp la zone de detection temporaire
	 */
	private void camera(Area detectionZoneTemp) {
		Area toAdd;
		double openingAngle = 45;

		Ellipse2D.Double camera = new Ellipse2D.Double(getPosition().getX() - CAMERA_RANGE, getPosition().getY() - CAMERA_RANGE, CAMERA_RANGE * 2, CAMERA_RANGE * 2);
		toAdd = new Area(camera);

		Path2D.Double angleRestriction = new Path2D.Double();
		angleRestriction.moveTo(getPosition().getX(), getPosition().getY());
		angleRestriction.lineTo(getPosition().getX() + Math.cos(getOrientation() + Math.toRadians(openingAngle / 2)) * 1.5 * CAMERA_RANGE, getPosition().getY() + Math.sin(-getOrientation() - Math.toRadians(openingAngle / 2)) * 1.5 * CAMERA_RANGE);
		angleRestriction.lineTo(getPosition().getX() + Math.cos(getOrientation() - Math.toRadians(openingAngle / 2)) * 1.5 * CAMERA_RANGE, getPosition().getY() + Math.sin(-getOrientation() + Math.toRadians(openingAngle / 2)) * 1.5 * CAMERA_RANGE);
		angleRestriction.closePath();
		toAdd.intersect(new Area(angleRestriction));

		detectionZoneTemp.add(toAdd);
	}

	/**
	 * reduit la zone de detection en fonction des obstacles
	 * 
	 * @param detectionZoneTemp
	 *            la zone de detection
	 */
	private void obstacleReduceDetection(Area detectionZoneTemp) {
		Area intersection = new Area();
		for (Obstacle obstacle : ScvAnimation.getObstacles()) {
			if (obstacle.getArea().getBounds().getLocation().distance(getPosition()) <= CAMERA_RANGE + Math.sqrt(Math.pow(obstacle.getArea().getBounds().getHeight(), 2) + Math.pow(obstacle.getArea().getBounds().getWidth(), 2))) {
				intersection = (Area) (obstacle.getArea()).clone();
				intersection.intersect(detectionZoneTemp);
				if (!intersection.isEmpty()) {
					obstacle.removeCommonArea(detectionZoneTemp);
					obstacle.removeHiddenAreaGeneric(detectionZoneTemp, getPosition());
				}
			}
		}
	}

	/**
	 * ajoute les informations de position de voiture des voitures contenues
	 * dans la zone de detection
	 */
	private void addCarPostionInformation() {

		for (ScvVehicle vehicle : ScvAnimation.getVehicles()) {
			if (detectionZone.contains(vehicle.getPosition())) {
				//upperLayer.addInformation(new CarPosition(ScvAnimation.getTime(), vehicle.getPosition(), vehicle.getSumoVehicle().getID()) );
				if (vehicle.isCrashed()) {
					upperLayer.addInformation(new PreCrashInformation(ScvAnimation.getTime(), vehicle, vehicle.getSumoVehicle().getID()));
				}
			}
		}
		upperLayer.addInformation(new CarPosition(ScvAnimation.getTime(), this.getPosition(), this.getSumoVehicle().getID()) );


	}

	/**
	 * dessine le DsrcVehicle
	 * 
	 * @param g2d
	 *            le Graphics2D qui dessinera le vehicule
	 * @param mat
	 *            la matrice de transformation servant a dessiner le vehicule
	 */
	@Override
	public void draw(Graphics2D g2d, AffineTransform mat) {
		drawGenericVehicle(g2d, mat, Color.RED);
	}

	/**
	 * dessine la zone de detection
	 * 
	 * @param g2d
	 *            le Graphics2D qui dessinera le vehicule
	 * @param mat
	 *            la matrice de transformation servant a dessiner le vehicule
	 */
	public void drawDetectionZone(Graphics2D g2d, AffineTransform mat) {
		drawVehicleArea(g2d, mat, TRANSPARENT_BLUE, detectionZone);
	}

	/**
	 * dessine la zone de propagation
	 * 
	 * @param g2d
	 *            le Graphics2D qui dessinera le vehicule
	 * @param mat
	 *            la matrice de transformation servant a dessiner le vehicule
	 */
	public void drawPropagationZone(Graphics2D g2d, AffineTransform mat) {
		Ellipse2D.Double propagationZone = new Ellipse2D.Double(getPosition().getX() - PROPAGATION_RANGE, getPosition().getY() - PROPAGATION_RANGE, PROPAGATION_RANGE * 2, PROPAGATION_RANGE * 2);
		Area propagationArea = new Area(propagationZone);
		drawVehicleArea(g2d, mat, TRANSPARENT_ORANGE, propagationArea);
	}

	/**
	 * dessine la zone d'acces a l'inforamtion du vehicule
	 * @param g2d le contexte graphique 2d
	 * @param mat la matrice de transformation
	 */
	public void drawInformationAccessZone(Graphics2D g2d, AffineTransform mat) {
		upperLayer.drawInformationAccessZone(g2d, mat);
	}

	/**
	 * dessine une aire translucide de couleur variable
	 * 
	 * @param g2d
	 *            le Graphics2D qui dessinera le vehicule
	 * @param mat
	 *            la matrice de transformation servant a dessiner le vehicule
	 * @param color
	 *            la couleur de l'aire a dessiner
	 * @param toPaint
	 *            l'aire a dessiner
	 */

	private void drawVehicleArea(Graphics2D g2d, AffineTransform mat, Color color, Area toPaint) {
		if (isDrawing()) {
			AreaDrawing.drawArea(g2d, mat, color, toPaint);
		}
	}
	
	/**
	 * met a jour les informations affichees du vehicule
	 * 
	 * @param listener l'ecouteur de mise a jour
	 */
	@Override
	public void carInformationUpdated(UpdateListener listener) {
		Double pts = VehicleTrustPoints.getMap().get(this.getSumoVehicle().getID());
		listener.carInformationUpdated(getPosition(), getSpeed(), physicLayer.getState(), physicLayer.getNoise(), pts);
	}

	/**
	 * retourne vrai car un DsrcVehicle est muni de dsrc sert a differiencer les
	 * DsrcVehicle avec les non-DSRC
	 * 
	 * @return true
	 */
	@Override
	public boolean isDsrc() {
		return true;
	}

	/**
	 * retourne le PhysicLayer
	 * 
	 * @return le PhysicLayer
	 */
	public PhysicLayer getPhysicLayer() {
		return physicLayer;
	}

	/**
	 * retourne le UpperLayer
	 * 
	 * @return le UpperLayer
	 */
	public UpperLayer getUpperLayer() {
		return upperLayer;
	}

	/**
	 * retourne la zone de detection
	 * 
	 * @return la zone de detection
	 */
	public Area getDetectionZone() {
		return detectionZone;
	}

	/** 
	 * defini si le vehicule est accidente
	 * @see vehicle.ScvVehicle#setCrashed(boolean)
	 **/
	@Override
	public void setCrashed(boolean crashed) {
		super.setCrashed(crashed);
		if (crashed) {
			ScvAnimation.addEvent(new DsrcWarning(ScvAnimation.getTime(), new PreCrashInformation(ScvAnimation.getTime(), this, getSumoVehicle().getID()),this));
		}
	}

	/**
	 * retourne les statistiques de connections directes du vehicule
	 * @return les statistiques de connections directes du vehicule
	 */
	public Statistic getDirectConnectionsStats() {
		return directConnectionSuccededStats;
	}

	/**
	 * ajoute une valeur a la liste de connection reussies
	 * @param time le temps de la valeur a rajouter
	 * @param connectionsSucceeded le nombre de connections reussies
	 */
	public void addDirectConnectionsSuccededStat(double time, int connectionsSucceeded) {
		directConnectionSuccededStats.addValue(new Point2D.Double(time, connectionsSucceeded));
	}

	/**
	 * retourne les statistiques de connections ratees a cause des obstacles (par Nakagami)
	 * @return le nombre de connections ratees a cause des obstacles
	 */
	public Statistic getDirectConnectionsFailedObstacleStats() {
		return directFailedConnectionsObstaclesStats;
	}
	
	/**
	 * retourne les statistiques de de connections ratees a cause de collision (par le bruit)
	 * @return le nombre de connections ratees a cause de collision
	 */
	public Statistic getDirectConnectionsFailedCollisionStats() {
		return directFailedConnectionsCollisionStats;
	}

	/**
	 * ajoute une valeur au nombre de connections ratees par collision et par obstacle
	 * @param time le temps des valeurs a ajouter
	 * @param connectionsFailedByObstacle le nombre de connections ratees pour cause d'obstacle
	 * @param connectionsFailedByCollision le nombre de connections ratees pour cause de collision
	 */
	public void addDirectConnectionsFailedStats(double time, int connectionsFailedByObstacle, int connectionsFailedByCollision) {

		directFailedConnectionsObstaclesStats.addValue(new Point2D.Double(time, connectionsFailedByObstacle));
		
		directFailedConnectionsCollisionStats.addValue(new Point2D.Double(time, connectionsFailedByCollision));
	}
	
	/**
	 * retourne le nombre de connection tentees par le vehicule
	 * @return le nombre de connection tentees par le vehicule
	 */
	public Statistic getDirectConnectionTriedStats() {
		return directConnectionTriedStats;
	}
	
	/**
	 * ajoute une valeur au nombre de connections tentees par le vehicule
	 * @param time le temps de la valeur a ajouter
	 * @param connectionsTried le nombre de connections tentees
	 */
	public void addDirectConnectionTriedStat(double time, int connectionsTried){
		directConnectionTriedStats.addValue(new Point2D.Double(time, connectionsTried));
	}

	/**
	 * retourne le taux de fiabilité des communications
	 * @return le taux de fiabilité des communications
	 */
	public Statistic getDirectConnectionSuceededRate() {
		return directConnectionSucceededRate;
	}

	/**
	 * ajoute une valeur au taux de fiabilité des communications
	 * @param time le temps de la valeur a ajouter
	 * @param rate le taux de fiabilité
	 */
	public void addDirectConnectionSucceededRate(double time, double rate) {
		directConnectionSucceededRate.addValue(new Point2D.Double(time,  rate));
	}

	/**
	 * retourne le bruit de la radio du vehicule
	 * @return le bruit de la radio du vehicule
	 */
	public Statistic getNoiseStats() {
		return noiseStats;
	}

	/**
	 * ajoute une valeur au bruit de la radio du vehicule
	 * @param time le temps de la valeur a ajouter
	 * @param noise le bruit a ajouter
	 */
	public void addNoiseStats(double time, double noise) {
		noiseStats.addValue(new Point2D.Double(time,noise));
	}

	public boolean isMalicious(){
		return malicious;
	}
	
}
