package vehicle;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import drawing.Drawable;
import it.polito.appeal.traci.Vehicle;
import listener.UpdateListener;

/**
 * Classe decrivant un vehicule generique
 * 
 * @author _Philippe_Cedryk
 *
 */
public class ScvVehicle implements Drawable {

	private Point2D.Double position;
	private Point2D.Double oldPosition;
	private Vehicle sumoVehicle;
	protected final static double CAR_DIAMETER = 3.5;
	private final double HOVER_DIAMETER = 5;
	private boolean drawing;
	private Ellipse2D.Double carCircle;
	private double orientation;
	private static boolean images;
	private static Image[] carImages;
	private boolean crashed;
	private static final int MAX_SPEED = 50;

	/**
	 * Cree un ScvVehicle
	 * 
	 * @param position
	 *            la position du vehicule
	 * @param sumoVehicle
	 *            vehicule de SUMO equivalent au vehicule SCV
	 */
	// auteur : philippe
	public ScvVehicle(Point2D.Double position, Vehicle sumoVehicle) {
		setDrawing(true);
		crashed = false;
		this.position = position;
		this.sumoVehicle = sumoVehicle;
		this.carCircle = new Ellipse2D.Double(position.getX() - CAR_DIAMETER / 2, position.getY() - CAR_DIAMETER / 2, CAR_DIAMETER, CAR_DIAMETER);
		orientation = 0;
		if (images)
			loadImages();
	}

	/**
	 * Utile afin de differiencer un DsrcVehicle d'un ScvVehicle
	 * 
	 * @return Retourne Faux s'il s'agit d'un ScvVehicle et Retourne Vrai s'il
	 *         s'agit d'un DsrcVehicle
	 */
	// auteur : philippe
	public boolean isDsrc() {
		return false;
	}

	/**
	 * Retourne la position du vehicule
	 * 
	 * @return la position du vehicule
	 */
	// auteur : philippe
	public Point2D.Double getPosition() {
		return position;
	}

	/**
	 * modifie la position du vehicule
	 * 
	 * @param position
	 *            la nouvelle position du vehicule
	 */
	// auteur : philippe
	public void setPosition(Point2D.Double position) {
		oldPosition = this.position;
		this.position = position;
		calculateOrientation();
		if (isDsrc()) {
			DsrcVehicle dsrcVehicle = (DsrcVehicle) this;
			dsrcVehicle.calculateDetectionZone();
			dsrcVehicle.getPhysicLayer().setPosition(position);
		}
	}

	/**
	 * retourne l'equivalent SUMO du vehicle de SCV
	 * 
	 * @return le vehicule SUMO
	 */
	// auteur : philippe
	public Vehicle getSumoVehicle() {
		return sumoVehicle;
	}

	/**
	 * dessine le vehicule
	 * 
	 * @param g2d
	 *            le Graphics2D qui sera utilise pour dessiner le vehicule
	 * @param mat
	 *            la matrice de transformation servant a dessiner le vehicule
	 */
	// auteur : philippe
	@Override
	public void draw(Graphics2D g2d, AffineTransform mat) {
		drawGenericVehicle(g2d, mat, Color.DARK_GRAY);
	}

	/**
	 * dessine le vehicule selectionne en rouge afin qu'il soit reconnaissable
	 * 
	 * @param g2d
	 *            le Graphics2D qui sera utilise pour dessiner le vehicule
	 * @param mat
	 *            la matrice de transformation servant a dessiner le vehicule
	 */
	// auteur : philippe
	public void drawSelected(Graphics2D g2d, AffineTransform mat) {
		drawGenericVehicle(g2d, mat, Color.GREEN);
	}

	/**
	 * methode generique qui permet de dessiner differents vehicule en changeant
	 * les valeurs entrees en parametre
	 * 
	 * @param g2d
	 *            le Graphics2D qui sera utilise pour dessiner le vehicule
	 * @param mat
	 *            la matrice de transformation servant a dessiner le vehicule
	 * @param color
	 *            la couleur utilisee pour dessiner le vehicule
	 */
	// auteur : philippe
	protected void drawGenericVehicle(Graphics2D g2d, AffineTransform mat, Color color) {
		if (isDrawing()) {
			updateCarCircle();
			if (isImages()) {
				drawImages(g2d, mat, color);
			} else {
				Color colorBefore = g2d.getColor();

				g2d.setColor(color);
				g2d.fill(mat.createTransformedShape(carCircle));
				g2d.setColor(colorBefore);
			}
		}
	}

	/**
	 * methode qui dessine les vehicules avec l'image appropri�e
	 * 
	 * @param g2d
	 *            le Graphics2D qui sera utilise pour dessiner le vehicule
	 * @param mat
	 *            la matrice de transformation servant a dessiner le vehicule
	 * @param color
	 *            la couleur utilisee pour dessiner le vehicule
	 */
	// auteur : cedryk
	private void drawImages(Graphics2D g2d, AffineTransform mat, Color color) {
		Point2D.Double realDestCorner1 = new Point2D.Double();
		Point2D.Double bottomLeftCorner = new Point2D.Double(position.getX() - CAR_DIAMETER, position.getY() + CAR_DIAMETER / 2);
		realDestCorner1 = (Double) mat.transform(bottomLeftCorner, realDestCorner1);

		Point2D.Double realDestCorner2 = new Point2D.Double();
		Point2D.Double topRightCorner = new Point2D.Double(position.getX() + CAR_DIAMETER, position.getY() - CAR_DIAMETER / 2);
		realDestCorner2 = (Double) mat.transform(topRightCorner, realDestCorner2);

		Point2D.Double realPosition = (Double) mat.transform(position, null);

		AffineTransform g2dInit = g2d.getTransform();

		g2d.rotate(orientation, realPosition.getX(), realPosition.getY());

		Image imageToDraw = null;
		if (color.equals(Color.GREEN)) {
			imageToDraw = carImages[0];
		} else if (color.equals(Color.DARK_GRAY)) {
			imageToDraw = carImages[1];
		} else {
			imageToDraw = carImages[2];
		}
		g2d.drawImage(imageToDraw, (int) realDestCorner1.getX(), (int) realDestCorner1.getY(), (int) Math.abs(realDestCorner2.getX() - realDestCorner1.getX()), (int) Math.abs(realDestCorner2.getY() - realDestCorner1.getY()), null);

		g2d.setTransform(g2dInit);

	}

	/**
	 * met a jour les informations affichees du vehicule
	 * 
	 * @param listener l'ecouteur de mise a jour
	 */
	public void carInformationUpdated(UpdateListener listener) {
		listener.carInformationUpdated(position, getSpeed(), null, -1, (double) -1);
	}

	/**
	 * @param g2d
	 *            le contexte graphique
	 * @param mat
	 *            la matrice de transformation
	 */
	// auteur: Philippe
	public void drawHighlight(Graphics2D g2d, AffineTransform mat) {
		Point2D.Double realDestCorner1 = new Point2D.Double();
		Point2D.Double bottomLeftCorner = new Point2D.Double(position.getX() - HOVER_DIAMETER, position.getY() + HOVER_DIAMETER / 2);
		realDestCorner1 = (Double) mat.transform(bottomLeftCorner, realDestCorner1);

		Point2D.Double realDestCorner2 = new Point2D.Double();
		Point2D.Double topRightCorner = new Point2D.Double(position.getX() + HOVER_DIAMETER, position.getY() - HOVER_DIAMETER / 2);
		realDestCorner2 = (Double) mat.transform(topRightCorner, realDestCorner2);

		Point2D.Double realPosition = (Double) mat.transform(position, null);

		AffineTransform g2dInit = g2d.getTransform();

		g2d.rotate(orientation, realPosition.getX(), realPosition.getY());

		g2d.drawImage(carImages[3], (int) realDestCorner1.getX(), (int) realDestCorner1.getY(), (int) Math.abs(realDestCorner2.getX() - realDestCorner1.getX()), (int) Math.abs(realDestCorner2.getY() - realDestCorner1.getY()), null);

		g2d.setTransform(g2dInit);
	}

	/**
	 * retourne un string decrivant le vehicule, soit le Id du vehicule SUMO
	 * 
	 * @return le Id du vehicule SUMO
	 */
	// auteur : philippe
	public String toString() {
		return sumoVehicle.getID();
	}

	/**
	 * Verifie si on devrait toujours dessiner ce vehicule
	 * 
	 * @return vrai si on doit le dessiner, sinon faux
	 */
	// auteur : cedryk
	public boolean isDrawing() {
		return drawing;
	}

	/**
	 * determine si on doit dessiner ce vehicule
	 * 
	 * @param drawing
	 *            vrai si oui, faux si non
	 */
	// auteur : cedryk
	public void setDrawing(boolean drawing) {
		this.drawing = drawing;
	}

	/**
	 * retourne le cercle qui defini ce qui est graphiquement represente comme
	 * le vehicule
	 * 
	 * @return le cercle dessine pour representer le vehicule
	 */
	// auteur : philippe
	public Ellipse2D.Double getCarCircle() {
		return carCircle;
	}

	/**
	 * met a jour la valeur du cercle dessine pour representer le vehicule
	 */
	// auteur : philippe
	public void updateCarCircle() {
		carCircle = new Ellipse2D.Double(position.getX() - CAR_DIAMETER / 2, position.getY() - CAR_DIAMETER / 2, CAR_DIAMETER, CAR_DIAMETER);
	}

	/**
	 * Retourne l'orientation du vehicule
	 * 
	 * @return l'orientation, en radians
	 */
	// auteur : cedryk
	public double getOrientation() {
		return orientation;
	}

	/**
	 * Change la valeur d'orientation du vehicule
	 * 
	 * @param orientation
	 *            l'orientation a changer
	 */
	// auteur : cedryk
	public void setOrientation(double orientation) {
		this.orientation = orientation;
	}

	/**
	 * Permet de savoir si un vehicule doit etre dessin� avec des images
	 * 
	 * @return true si le vehicule est dessin� avec les images et false sinon
	 */
	// auteur : philippe
	public static boolean isImages() {
		return images;
	}

	/**
	 * Dessiner un vehicule avec des images ou non
	 * 
	 * @param images
	 *            true si le vehicule doit etre dessin� avec des images, false
	 *            sinon
	 */
	// auteur : philippe
	public static void setImages(boolean images) {
		ScvVehicle.images = images;
	}

	/**
	 * charge les images
	 */
	// auteur : cedryk et philippe
	public void loadImages() {
		if (carImages == null) {
			carImages = new Image[4];
			URL imgUrl1 = getClass().getClassLoader().getResource("greencar.png");
			URL imgUrl2 = getClass().getClassLoader().getResource("graycar.png");
			URL imgUrl3 = getClass().getClassLoader().getResource("redcar.png");
			URL imgUrl4 = getClass().getClassLoader().getResource("highlight.png");
			try {
				carImages[0] = ImageIO.read(imgUrl1);
				carImages[1] = ImageIO.read(imgUrl2);
				carImages[2] = ImageIO.read(imgUrl3);
				carImages[3] = ImageIO.read(imgUrl4);

			} catch (IOException e) {
				System.out.println("Erreur de chargement d'image");
			}
		}
	}

	/**
	 * Calcule l'orientation du v�hicule
	 */
	// auteur : cedryk
	private void calculateOrientation() {
		if (oldPosition != null) {
			double dx = position.getX() - oldPosition.getX();
			double dy = -position.getY() + oldPosition.getY();
			if (!java.lang.Double.isNaN(dy / dx)) {
				orientation = Math.atan(dy / dx);

				if ((orientation < 0 && dy > 0) || (orientation > 0 && dy < 0)) {
					orientation += Math.PI;
				} else if (orientation == 0 && dx < 0) {
					orientation += Math.PI;
				}
			}

		} else {
			orientation = 0;
		}
	}

	/**
	 * Retourne la vitesse courante du v�hicule
	 * 
	 * @return La vitesse du v�hicule, en m�tres par seconde
	 */
	// auteur : cedryk
	public double getSpeed() {
		double speed = -1;
		try {
			speed = sumoVehicle.getSpeed();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return speed;
	}

	/**
	 * verifie si le vehicule est accidente
	 * 
	 * @return vrai si le vehicule est accidente, faux sinon
	 */
	public boolean isCrashed() {
		return crashed;
	}

	/**
	 * defini si le vehicule est accidente
	 * 
	 * @param crashed
	 *            vrai si le vehicule est accidente, faux sinon
	 */
	public void setCrashed(boolean crashed) {
		this.crashed = crashed;
	}

	/**
	 * retourne le diametre du vehicule
	 * 
	 * @return le diametre du vehicule
	 */
	public static double getCarDiameter() {
		return CAR_DIAMETER;
	}

	/**
	 * retourne la vitesse maximale du vehicule
	 * 
	 * @return la vitesse maximale du vehicule
	 */
	public static int getMaxSpeed() {
		return MAX_SPEED;
	}

}
