package vehicle;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import aapplication.ScvAnimation;
import drawing.AreaDrawing;
import information.CarPosition;
import information.Information;
import information.ZoneInformation;

/**
 * La couche superieure du vehicule, gere les informations connues par le
 * vehicule
 * 
 * @author _Philippe_
 *
 */
public class UpperLayer {

	private List<Information> information;
	private Area informationAccessZone;
	private Area detectionZone;
	private String sumoId;
	private HashMap<String, java.lang.Double> trustMap;
	private static final Color TRANSPARENT_MAGENTA = new Color((float) (Color.MAGENTA.getRed() / 255.0),
			(float) (Color.MAGENTA.getGreen() / 255.0), (float) (Color.MAGENTA.getBlue() / 255.0), (float) 0.3);

	/**
	 * cree un UpperLayer
	 * 
	 * @param sumoId
	 *            l'identifiant du vehicule
	 */
	public UpperLayer(String sumoId) {
		this.information = Collections.synchronizedList(new ArrayList<Information>());
		this.sumoId = sumoId;
		trustMap = new HashMap<String, java.lang.Double>();
	}

	/**
	 * ajoute une information a la liste d'information connue par
	 * 
	 * @param info
	 *            l'information a ajouter
	 */
	public void addInformation(Information info) {
		if (!this.information.contains(info)) {
			ArrayList<Information> toRemove = new ArrayList<Information>();
			for (Information infoTemp : information) {
				if (infoTemp.getId().equals(info.getId())) {
					toRemove.add(infoTemp);
					break;
				}
			}
			information.removeAll(toRemove);
			this.information.add(info);
		}

	}

	/**
	 * ajoute une liste d'informations a la liste d'informations connue par le
	 * vehicule
	 * 
	 * @param info
	 *            la liste d'information a rajouter
	 */
	public void addInformation(List<Information> info) {
		for (Information informationReceived : info) {
			String childId = informationReceived.getId().substring(0, informationReceived.getId().length() - 1);
			if (!trustMap.containsKey(childId) || (trustMap.containsKey(childId) && trustMap.get(childId) > 0.5)) {

				ArrayList<Information> toRemove = new ArrayList<Information>();
				if (childId != this.sumoId) {
					for (Information knownInfo : information) {
						if (knownInfo.getId().equals(informationReceived.getId())) {
							toRemove.add(knownInfo);
							break;
						}
					}
					information.removeAll(toRemove);
					addInformation(informationReceived);
				}

			}
		}
	}

	/**
	 * dessine la zone d'acces a l'information
	 * 
	 * @param g2d
	 *            le Graphics2D qui dessinera le vehicule
	 * @param mat
	 *            la matrice de transformation servant a dessiner le vehicule
	 */
	public void drawInformationAccessZone(Graphics2D g2d, AffineTransform mat) {
		informationAccessZone = new Area();
		ZoneInformation zoneInfo;
		synchronized (information) {
			for (Information info : information) {
				if (info.getClass().equals(ZoneInformation.class)) {
					zoneInfo = (ZoneInformation) info;
					informationAccessZone.add(zoneInfo.getInformationAccessZone());
				}
			}
		}
		AreaDrawing.drawArea(g2d, mat, TRANSPARENT_MAGENTA, informationAccessZone);
	}

	/**
	 * Mets a jour la zone d'information de detection de la voiture
	 */
	public void updateDetectionZoneInformation() {
		addInformation(new ZoneInformation(ScvAnimation.getTime(), detectionZone, sumoId));
	}

	/**
	 * retourne la zone de detection
	 * 
	 * @return la zone de detection
	 */
	public Area getDetectionZone() {
		return detectionZone;
	}

	/**
	 * permet de definir la zone de detection
	 * 
	 * @param detectionZone
	 *            la nouvelle zone de detection
	 */
	public void setDetectionZone(Area detectionZone) {
		this.detectionZone = detectionZone;
		this.updateDetectionZoneInformation();
	}

	/**
	 * retourne la liste d'informations connues par le v�hicule
	 * 
	 * @return la lisre d'informations
	 */
	public List<Information> getInformations() {
		return information;
	}

	public void sendTrustMap(HashMap<String, java.lang.Double> map) {
		trustMap = map;
	}
}
