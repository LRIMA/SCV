package event;

import java.util.ArrayList;
import java.util.List;

import aapplication.ScvAnimation;
import connection.Connection;
import connection.ConnectionManager;
import infrastructure.Antenna;
import physics.Nakagami;
import vehicle.PhysicLayer;
import vehicle.VehicleRadioState;

/**
 * Classe abstraite permettant de mieux organiser les evenements dsrc et
 * non-dsrc
 * 
 * @author Cedryk
 *
 */
public abstract class DsrcInfra extends Event {

	/**
	 * duree d'une transmission Dsrc
	 */
	protected static int messageDuration = 30;
	private static int DELAY= 10;
	private ArrayList<PhysicLayer> noisedVehicles;
	private ArrayList<Double> noise;
	private Connection connection;
	private List<PhysicLayer> receivers;

	/**
	 * vrai s'il s'agit du d�but d'un message, faux s'il s'agit d'une fin
	 */
	private boolean begining;

	protected Antenna sender;

	/**
	 * cree un evenement en specifiant son temps
	 * 
	 * @param time
	 *            le temps de l'evenement
	 * @param sender le vehicule qui envoie le message
	 */
	public DsrcInfra(int time, Antenna sender) {
		super(time);
		begining = true;
		this.sender = sender;
	}

	/**
	 * Change l'�v�nement en �v�nement de d�but on de fin
	 * 
	 * @param isBegining
	 *            si l'�v�nement d�bute ou non
	 */
	public void setBegining(boolean isBegining) {
		begining = isBegining;
	}

	/**
	 * retourne un bool�en exprimant si l'�v�nement est un �v�nement de d�but ou
	 * non
	 * 
	 * @return true si l'�v�nement d�bute et false sinon
	 */
	public boolean isBegining() {
		return begining;
	}

	/**
	 * verifie si le mode de la radio permet de debuter une communication Dsrc
	 * @return vrai si la communication Dsrc peut avoir lieu, faux sinon
	 */
	private boolean canStartRun() {
		if (sender.getPhysicLayer().getState().equals(VehicleRadioState.LISTENING)||sender.getPhysicLayer().getState().equals(VehicleRadioState.SENDING)) {
			delay();
			return false;	
		}else{
			sender.getPhysicLayer().setState(VehicleRadioState.SENDING);
			return true;
		}
	}
	
	/**
	 * repousse le temps de l'evenement dsrc d'une duree DELAY
	 */
	public void delay(){
		this.setTime(getTime()+DELAY);
		ScvAnimation.addEvent(this);
	}
	
	/**
	 * @see event.Event#run()
	 * 
	 **/
	public void run() {

		if (isBegining()) {
			runBeggining();
		} else {
			runEnd();
		}
	}
	
	/**
	 * debute la communication Dsrc
	 */
	protected void runBeggining(){
		if (canStartRun()) {
			double noiseTemp;
			noisedVehicles = new ArrayList<PhysicLayer>();
			ArrayList<PhysicLayer> toRemove = new ArrayList<PhysicLayer>();
			noise = new ArrayList<Double>();
			ConnectionManager manager = ScvAnimation.getConnectionManager();
			connection = manager.findConnection(sender.getPhysicLayer());
			connection.calculateReceivers();
			receivers = connection.getReceivers();
			//double connectionsTried = receivers.size();
			//sender.addDirectConnectionTriedStat(getTime()/1000.0, (int) connectionsTried);
			
			synchronized (receivers) {
				for (PhysicLayer receiver : receivers) {
					noiseTemp = receiver.canReadNoisePossibility(Nakagami.calculateMessagePowerReceived(sender.getPosition(),receiver.getPosition()));
					if (noiseTemp > 0) {
						noisedVehicles.add(receiver);
						noise.add(noiseTemp);
					} else if (noiseTemp == -1) {
						toRemove.add(receiver);
					}
				}
				receivers.removeAll(noisedVehicles);
				receivers.removeAll(toRemove);
			}
			/*sender.addDirectConnectionsSuccededStat(getTime()/1000.0, receivers.size());
			sender.addDirectConnectionsFailedStats(getTime()/1000.0, toRemove.size(), noisedVehicles.size());
			sender.addDirectConnectionSucceededRate(getTime()/1000.0, 100*receivers.size()/connectionsTried);*/
			this.setBegining(false);
			this.setTime(this.getTime() + messageDuration);
			ScvAnimation.addEvent(this);
		}
	}
	
	/**
	 * termine la communication Dsrc
	 */
	protected void runEnd(){
		synchronized (receivers) {
			for (PhysicLayer receiver : receivers) {
				if (receiver.canRead(Nakagami.calculateMessagePowerReceived(sender.getPosition(), receiver.getPosition()))) {
					infoTransfer(receiver);
				}
				receiver.setState(VehicleRadioState.WAITING);
			}
		}
		for (PhysicLayer noisedVehicle : noisedVehicles) {
			noisedVehicle.removeNoise(noise.get(noisedVehicles.indexOf(noisedVehicle)));
		}
		sender.getPhysicLayer().setState(VehicleRadioState.WAITING);
		
		if(this.getClass()==DsrcPeriodicInfra.class){
			DsrcPeriodicInfra dsrcPeriodic = (DsrcPeriodicInfra) this;
			dsrcPeriodic.addNextPeriodic();
		}
	}
	
	/**
	 * transfere les informations du message au recepteur
	 * @param receiver le vehicule qui recoit les informations
	 */
	protected abstract void infoTransfer(PhysicLayer receiver);
	
	public String toString(){
		return super.toString() + " : " +this.sender.toString();
	}
	
}
