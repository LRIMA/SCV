package event;

import java.awt.geom.Point2D.Double;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import aapplication.ScvAnimation;
import connection.ConnectionManager;
import infrastructure.VehicleTrustPoints;
import it.polito.appeal.traci.Vehicle;
import vehicle.DsrcVehicle;
import vehicle.ScvVehicle;

/**
 * Classe repr�sentant un �v�nement de mise � jour des positions des voitures,
 * d�rivant de Event
 * 
 * @see Event
 * @author _Cedryk_
 *
 */
public class Update extends Event {

	private static int stepLength;
	private static double dsrcPercentage = 0.5;
	private static int currDsrcVehicles;
	private static double maliciousPercentage = 0.5;
	private static int currMaliciousVehicles;

	/**
	 * Cr�e un �v�nement update en sp�cifiant le temps
	 * 
	 * @param time
	 *            temps auquel d�marrer l'�v�nement
	 */
	public Update(int time) {
		super(time);
	}

	/**
	 * D�marre l'�v�nement de mise � jour et met � jour les positions des
	 * voitures
	 * 
	 */
	public void run() {
		Collection<Vehicle> sumoVehicles;
		List<ScvVehicle> vehicles = ScvAnimation.getVehicles();
		ConnectionManager manager = ScvAnimation.getConnectionManager();
		try {
			ScvAnimation.getTraciConnection().nextSimStep();
			sumoVehicles = ScvAnimation.getTraciConnection().getVehicleRepository().getAll().values();
			updateVehiclesPosition(sumoVehicles, vehicles, manager);
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}
		if (!ScvAnimation.getVehicles().isEmpty())
			ScvAnimation.addEvent(new Update(this.getTime() + stepLength));

	}

	/**
	 * Mets � jour les positions des voitures
	 * 
	 * @param sumoVehicles
	 *            collection de v�hicules Sumo
	 * @param vehicles
	 *            liste de v�hicules Scv
	 * @param manager le connectionManager de l'application
	 */
	private void updateVehiclesPosition(Collection<Vehicle> sumoVehicles, List<ScvVehicle> vehicles,
			ConnectionManager manager) {
		DsrcVehicle dsrcVehicle;
		boolean alreadyPresent;
		for (Vehicle vehicle : sumoVehicles) {
			alreadyPresent = false;
			for (ScvVehicle scvVehicle : vehicles) {
				if (vehicle.getID().equals(scvVehicle.getSumoVehicle().getID())) {
					alreadyPresent = true;
					try {
						scvVehicle.setPosition((Double) vehicle.getPosition());
						if(scvVehicle.isDsrc()){
							((DsrcVehicle)scvVehicle).addNoiseStats(getTime()/1000.0,((DsrcVehicle)scvVehicle).getPhysicLayer().getNoise());
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else if (!sumoVehicles.contains(scvVehicle.getSumoVehicle())) {
					scvVehicle.setDrawing(false);
				}
			}
			if (!alreadyPresent) {
				try {
					synchronized (vehicles) {
						if (nextVehicleIsDsrc(sumoVehicles, vehicles)) {
							boolean malicious = nextVehicleIsMalicious(sumoVehicles, vehicles);
							dsrcVehicle = new DsrcVehicle((Double) vehicle.getPosition(), vehicle,
									ScvAnimation.getTime() / 1000.0, malicious);
							vehicles.add(dsrcVehicle);
							manager.addConnection(dsrcVehicle);
							VehicleTrustPoints.addVehicle(dsrcVehicle.getSumoVehicle().getID(), getTime());
							Event periodicEvent = new DsrcPeriodic(this.getTime() + 1, dsrcVehicle);
							ScvAnimation.addEvent(periodicEvent);
						} else {
							ScvVehicle scvVehicle = new ScvVehicle((Double) vehicle.getPosition(), vehicle);
							vehicles.add(scvVehicle);
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private boolean nextVehicleIsMalicious(Collection<Vehicle> sumoVehicles, List<ScvVehicle> vehicles) {
		if (currMaliciousVehicles < sumoVehicles.size() * maliciousPercentage) {
			if (Math.random() <= maliciousPercentage) {
				currMaliciousVehicles++; 
				return true;
			} else {
				return false;
			}
		} else if(maliciousPercentage == -1 && currMaliciousVehicles < 1){
			currMaliciousVehicles++;
			return true;
		}else{
			return false;
		}
	}

	/**
	 * M�thode permettant de d�terminer si le prochain v�hicule devrait avoir la
	 * technologie DSRC ou non
	 * 
	 * @param sumoVehicles
	 *            la liste de v�hicules de Traci
	 * @param vehicles
	 *            la liste de v�hicules d�j� cr��s
	 * @return true si le prochain v�hicule doit �tre DSRC et false sinon
	 */
	private boolean nextVehicleIsDsrc(Collection<Vehicle> sumoVehicles, List<ScvVehicle> vehicles) {
		if (currDsrcVehicles < sumoVehicles.size() * dsrcPercentage) {
			if (Math.random() <= dsrcPercentage) {
				currDsrcVehicles++; 
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Set le temps du pas de simulation
	 * 
	 * @param length
	 *            le temps du pas de simulation
	 */
	public static void setStepLength(int length) {
		stepLength = length;
	}

	/**
	 * Change le pourcentage de vehicules utilisant la technologie dsrc
	 * 
	 * @param dsrcPercentageToChange le pourcentage de vehicule dote de Dsrc
	 */
	public static void setDsrcPercentage(int dsrcPercentageToChange) {
		dsrcPercentage = dsrcPercentageToChange / 100.0;
	}

	/**
	 * Remets a 0 le nombre de vehicules dsrc
	 */
	public static void resetCurrDsrcVehicles() {
		currDsrcVehicles = 0;
	}

	public static void setMaliciousPercentage(int maliciousPercentageToChange) {
		if(maliciousPercentageToChange != -1){
		maliciousPercentage = maliciousPercentageToChange / 100.0;
		}else{
			maliciousPercentage = maliciousPercentageToChange;
		}
	}

	public static int getCurrDsrcVehicles(){
		return currDsrcVehicles;
	}
}
