package event;

import aapplication.ScvAnimation;
import infrastructure.Antenna;
import vehicle.PhysicLayer;

/**
 * Classe derivant de Event et qui represente un evenement d'envoi de messages
 * d'un vehicule
 * 
 * @author _cedryk_
 *
 */
public class DsrcPeriodicInfra extends DsrcInfra {

	private static int messagesFrequency = 100;

	/**
	 * cree un evenement en specifiant son temps et le vehicule envoyant les
	 * messages
	 * 
	 * @param time
	 *            le temps de l'evenement
	 * @param sender
	 *            le vehicule qui envoie les messages
	 */
	public DsrcPeriodicInfra(int time, Antenna sender) {
		super(time, sender);
	}

	/**
	 * Envoie les informations d'un v�hicule � un de ses receivers
	 * 
	 * @param receiver
	 *            le v�hicules qui recoivent les informations
	 * @param sendingVehicle
	 *            le v�hicule qui envoie ses informations
	 */
	private void sendInformations(PhysicLayer receiver, Antenna sendingAntenna) {
		if(receiver.getType() == "vehicle"){
			receiver.getUpperLayer().sendTrustMap(sendingAntenna.getMap());
		}
		receiver.getUpperLayer().addInformation(sendingAntenna.getUpperLayer().getInformations());
	}

	/**
	 * Change la fr�quence d'envoi de messages
	 * 
	 * @param messagesFrequencyToChange
	 *            Le temps entre chaque envoi de messages, en ms
	 */
	public static void setMessagesFrequency(int messagesFrequencyToChange) {
		messagesFrequency = messagesFrequencyToChange;
	}

	/**
	 * ajoute a la liste d'evenement le prochain message periodic du vehicule
	 */
	protected void addNextPeriodic() {
		Event eventToAdd = new DsrcPeriodicInfra(this.getTime() + messagesFrequency, sender);
		ScvAnimation.addEvent(eventToAdd);
	}

	/**
	 * @see event.Dsrc#infoTransfer(vehicle.PhysicLayer)
	 **/
	@Override
	protected void infoTransfer(PhysicLayer receiver) {
		sendInformations(receiver, sender);

	}

}
