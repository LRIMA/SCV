package listener;

import java.util.EventListener;

/**
 * Listener permettant d'afficher la fenetre principale lors de la fermeture de la fenetre de presentation
 * @author _Philippe_
 *
 */
public interface PresentationWindowClosedListener extends EventListener{

	/**
	 * sera defini comme une methode qui affiche la fenetre principale
	 */
	public void presentationWindowClosed();
}
