package infrastructure;

import java.util.HashMap;

import aapplication.ScvAnimation;

public abstract class VehicleTrustPoints {
	private static final double INITIAL_POINTS = 1;
	private static final double REGAIN_POINTS_TRESHOLD = 1000;
	private static HashMap<String, Double> trustMap;
	private static HashMap<String, Double> timeMap;
	
	public static void initializeTrustPoints(){
		trustMap = new HashMap<String, Double>();
		refreshMap();
		timeMap = new HashMap<String, Double>();
	}
	
	public static void adjustPoints(String sumoId, int nbErrors, double time) {
		double pts = 0;
		if(nbErrors > 0){
			double ptsIni = trustMap.get(sumoId);
			//pts = ptsIni - (double)(nbErrors)*ptsIni/(double)(nbErrors+1);
			pts = ptsIni - 0.0001*(nbErrors+1)/nbErrors;
			trustMap.put(sumoId, Math.max(0, pts));
			timeMap.put(sumoId, time);
		}else{
			double timeSpent = time - timeMap.get(sumoId);
			double ptsIni = trustMap.get(sumoId);
			if(timeSpent >= REGAIN_POINTS_TRESHOLD && ptsIni > 30){
				pts = ptsIni + 0.1;
				trustMap.put(sumoId, Math.min(1, pts));
				timeMap.put(sumoId, time);
			}
		}
		refreshMap();
		
	}
	
	public static void addVehicle(String sumoId, double time){
		trustMap.put(sumoId, INITIAL_POINTS);
		refreshMap();
		timeMap.put(sumoId, time);
	}
	
	private static void refreshMap(){
		for(Antenna ant : ScvAnimation.getAntennas()){
			ant.getUpperLayer().sendTrustMap(trustMap);
		}
	}
	public static HashMap<String, Double> getMap(){
		return trustMap;
	}
}
